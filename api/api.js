export const HERO_LIST = 'https://api.opendota.com/api/heroes';
export const appdescription = 'api/appinfo/appdescription';

export const register = 'api/users/register';
export const login = 'api/users/login';
export const autoLoginCheck = 'api/users/autoLoginCheck';

export const postNew = 'api/userAction/postNew';
export const editPost = 'api/userAction/editPost';
export const deletePost = 'api/userAction/deletePost';
export const getPost = 'api/post/getPost';
export const getSinglePost = 'api/post/getSinglePost';
export const postComment = 'api/userAction/postComment';
export const postReply = 'api/userAction/postReply';

export const uploadProfilePhoto = 'api/upload/uploadProfilePhoto';

export const searchAll = 'api/items/searchAll';
export const getAllTopic = 'api/topics/alltopics';

export const getProfilePhoto = 'api/loadProfilePhoto/';
export const getImage = 'api/loadImage/';
import React from 'react';
import {View} from 'react-native'
import ScreenNavigator from './navigator/screenNavigator';
import LoadingScreen from './component/loadingScreen'

export default function App() {
  // let app = <View></View>
  return (
    <View style={{flex:1}}>
      <ScreenNavigator/>
      {/* <LoadingScreen load={true}/> */}
    </View>
  );
  // return app;
}
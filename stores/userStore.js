import { observable } from 'mobx';

export default userStore = observable({
    name:"",
    username:"",
    email:"",
    phone:"",
    token:"",
    id:"",
    profilePhoto:"",
});
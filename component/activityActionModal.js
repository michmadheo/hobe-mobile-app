import React, { Component } from 'react';
import { Text, View, Button, TouchableOpacity, Image, Modal, TouchableWithoutFeedback } from 'react-native';
import * as COLOR from "../src/colors"
import { hobeLogo } from '../src/images';
import LottieView from  'lottie-react-native';
import { paperPlane_Anim} from '../src/images';
import modalStore from '../stores/modalStore';

export default class ActivtyActionModal extends Component {

  static defaultProps ={
    title: "Title",
    headerStyle: {
    },
    textHello: {
      marginLeft:15, 
      color:COLOR.HITAM_REDUP, 
      fontSize:23, 
      paddingVertical:10, 
      fontWeight:'bold'}
  }

  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  render() {
    return (
        <Modal
        animationType="fade"
        transparent={true}
        onRequestClose={this.props.close}
        visible={this.props.load}
        >
            <TouchableWithoutFeedback onPress={this.props.close} style={{flex:1}}>
                <View style={{flex:1, justifyContent:'center', alignItems:'center', backgroundColor:'rgba(0, 0, 0, 0.3)'}}>
                    {/* <LottieView source={paperPlane_Anim} style={{width:250, height:250}} autoPlay loop/> */}
                    <View style={{width:200,backgroundColor:COLOR.PUTIH, borderRadius: 10}}>
                        {this.props.yourPost === true?
                        <View>
                            <TouchableOpacity onPress={this.props.actionEdit} style={buttonStyle}>
                                <Text style={textStyle}>Edit</Text>
                            </TouchableOpacity>
                            <View style={{height:2, backgroundColor:COLOR.ABU_TERANG}}/>
                            <TouchableOpacity onPress={this.props.actionDelete} style={buttonStyle}>
                                <Text style={textStyle}>Delete</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        <TouchableOpacity onPress={this.props.actionReport} style={buttonStyle}>
                            <Text style={textStyle}>Report</Text>
                        </TouchableOpacity>
                        }
                        {/* <TouchableOpacity onPress={()=>{alert('wow')}} style={buttonStyle}>
                            <Text style={textStyle}>wew</Text>
                        </TouchableOpacity> */}
                </View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    );
  }
}

const textStyle={
    fontSize:16
}

const buttonStyle={
    alignItems:'center', 
    paddingVertical:20
}
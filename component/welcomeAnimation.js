import React, { Component } from 'react';
import { Text, View, Button, TouchableOpacity, Image } from 'react-native';
import * as COLOR from "../src/colors"
import { gardening_Anim, cooking_Anim, skating_Anim, watching_Anim, hobeLogo } from '../src/images';
import LottieView from 'lottie-react-native';

export default class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
        renderAnimation: Math.floor(Math.random() * 4)
    };
  }

  render() {
    return (
        <LottieView style={{height:250, alignSelf:'center'}} source={this.animationSelector()} autoPlay loop/>
    );
  }

  animationSelector(){
      if(this.state.renderAnimation === 0){
          return gardening_Anim
      }
      if(this.state.renderAnimation === 1){
        return cooking_Anim
    }
    if(this.state.renderAnimation === 2){
        return skating_Anim
    }
    if(this.state.renderAnimation === 3){
        return watching_Anim
    }
  }
}
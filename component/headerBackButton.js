import React, { Component } from 'react';
import { Text, View, Button, TouchableOpacity, Image } from 'react-native';
import {backIcon, backIconWhite} from '../src/images';
import * as COLOR from "../src/colors";

export default class HeaderBackButton extends Component {

  static defaultProps ={
    borderless:false,
    title: "",
    headerStyle: {
    },
    actionButton: ()=>{},
    secondActionButton: ()=>{},
    textHello: {
      marginLeft:15, 
      color:COLOR.HITAM_REDUP, 
      fontSize:18, 
      paddingVertical:10
    },
    iconSize: {
      marginLeft:10, 
      width:25, 
      height:25,
      marginVertical:10,},
    headerBorder:{
      backgroundColor:COLOR.PUTIH, 
      shadowColor: "#000", 
      paddingVertical:6,
      shadowOffset: {
        width: 0,
        height: 2,},
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,},
    headerBorderless:{
      backgroundColor:COLOR.PUTIH,
      paddingVertical:6},
    textButton: { 
      color:COLOR.HITAM_REDUP, 
      fontSize:15, 
      paddingVertical:10},
  }

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={this.props.borderless?this.props.headerBorderless:this.props.headerBorder}>
        <View
        style={{flexDirection:'row'}}>
          <View style={{flexDirection:'row', flex:1}}>
            <TouchableOpacity onPress={this.props.actionButton}>
            <Image source={backIcon} style={this.props.iconSize}/>
            </TouchableOpacity>
            <View style={{justifyContent:'center'}}>
              <Text style={this.props.textHello}>{this.props.title}</Text>
            </View>
          </View>
          <TouchableOpacity style={{justifyContent:'center', marginRight:25}} onPress={this.props.secondActionButton}>
            <Text style={this.props.textButton}>Post</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
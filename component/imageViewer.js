import React, { Component } from 'react';
import { Text, View, Button, TouchableOpacity, Image, Modal, TouchableWithoutFeedback } from 'react-native';
import * as COLOR from "../src/colors"
import { hobeLogo } from '../src/images';
import LottieView from  'lottie-react-native';
import { paperPlane_Anim} from '../src/images';
import modalStore from '../stores/modalStore';
import { serverAPI } from '../server-configuration/server';
import { getImage, getProfilePhoto } from '../api/api';

export default class ImageViewer extends Component {

  static defaultProps ={
    title: "Title",
    headerStyle: {
    },
    textHello: {
      marginLeft:15, 
      color:COLOR.HITAM_REDUP, 
      fontSize:23, 
      paddingVertical:10, 
      fontWeight:'bold'},
    profilePhoto:false
  }

  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  static close = () =>{this.setState({show:false})}

  static show = () =>{modalStore.loadingModal=true}

  test(){
    alert('wow')
  }


  render() {
    return (
        <Modal
        animationType="fade"
        transparent={true}
        onRequestClose={this.props.close}
        visible={this.props.load}
        >
            <View style={{flex:1, justifyContent:'center', alignItems:'center', backgroundColor:'rgba(0, 0, 0, 0.9)'}}>
                <TouchableWithoutFeedback onPress={this.props.close}>
                <Image source={{uri: this.props.profilePhoto === true?serverAPI+getProfilePhoto+this.props.image:
                serverAPI+getImage+this.props.image}} style={{resizeMode:'contain', width:'100%', height:500}}/>
                </TouchableWithoutFeedback>
            </View>
        </Modal>
    );
  }
}
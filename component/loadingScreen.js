import React, { Component } from 'react';
import { Text, View, Button, TouchableOpacity, Image, Modal } from 'react-native';
import * as COLOR from "../src/colors"
import { hobeLogo } from '../src/images';
import LottieView from  'lottie-react-native';
import { paperPlane_Anim} from '../src/images';
import modalStore from '../stores/modalStore';

export default class LoadingScreen extends Component {

  static defaultProps ={
    title: "Title",
    headerStyle: {
    },
    textHello: {
      marginLeft:15, 
      color:COLOR.HITAM_REDUP, 
      fontSize:23, 
      paddingVertical:10, 
      fontWeight:'bold'}
  }

  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  static close = () =>{this.setState({show:false})}

  static show = () =>{modalStore.loadingModal=true}

  test(){
    alert('wow')
  }


  render() {
    return (
        <Modal
        animationType="fade"
        transparent={true}
        onRequestClose={()=>{
          this.setState({
            topicModal:false
          })
        }}
        visible={this.props.load}
        >
            <View style={{flex:1, justifyContent:'center', alignItems:'center', backgroundColor:'rgba(0, 0, 0, 0.3)'}}>
                <LottieView source={paperPlane_Anim} style={{width:250, height:250}} autoPlay loop/>
            </View>
        </Modal>
    );
  }
}
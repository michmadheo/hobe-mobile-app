import React, { Component } from 'react';
import { Text, View, Button, TouchableOpacity, Image, Modal, TouchableWithoutFeedback } from 'react-native';
import * as COLOR from "../src/colors"
import { hobeLogo } from '../src/images';
import LottieView from  'lottie-react-native';
import { paperPlane_Anim} from '../src/images';
import modalStore from '../stores/modalStore';

export default class ConfirmationModal extends Component {

  static defaultProps ={
    title: "Title",
    headerStyle: {
    },
    textHello: {
      marginLeft:15, 
      color:COLOR.HITAM_REDUP, 
      fontSize:23, 
      paddingVertical:10, 
      fontWeight:'bold'}
  }

  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  render() {
    return (
        <Modal
        transparent={true}
        onRequestClose={this.props.close}
        visible={this.props.load}
        >
            <TouchableWithoutFeedback onPress={this.props.close} style={{flex:1}}>
                <View style={{flex:1, justifyContent:'center', alignItems:'center', backgroundColor:'rgba(0, 0, 0, 0.3)'}}>
                    {/* <LottieView source={paperPlane_Anim} style={{width:250, height:250}} autoPlay loop/> */}
                    <View style={{width:200,backgroundColor:COLOR.PUTIH, borderRadius: 10, alignItems:'center'}}>
                        <Text style={{fontSize:18, paddingTop:20}}>{this.props.text}</Text>
                        <View style={{flexDirection:'row'}}>
                            <TouchableOpacity onPress={this.props.actionAccept} style={buttonStyle}>
                                <Text style={textStyleYes}>Accept</Text>
                            </TouchableOpacity>
                            <View style={{height:2, backgroundColor:COLOR.ABU_TERANG}}/>
                            <TouchableOpacity onPress={this.props.actionCancel} style={buttonStyle}>
                                <Text style={textStyleNo}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                </View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    );
  }
}

const textStyleYes={
    fontSize:16,
    color:COLOR.TEMA_UTAMA
}

const textStyleNo={
    fontSize:16
}

const buttonStyle={
    flex:1,
    alignItems:'center', 
    paddingVertical:20,
    marginHorizontal:20
}
import React, { Component } from 'react';
import { Text, View, Button, TouchableOpacity, Image } from 'react-native';
import * as COLOR from "../src/colors"
import { hobeLogo } from '../src/images';

export default class Header extends Component {

  static defaultProps ={
    title: "Title",
    headerStyle: {
    },
    textHello: {
      marginLeft:15, 
      color:COLOR.HITAM_REDUP, 
      fontSize:23, 
      paddingVertical:10, 
      fontWeight:'bold'}
  }

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View>
        <View
        style={this.props.headerStyle}>
          <Image source={hobeLogo} style={{resizeMode:'contain',height:40, width:80, marginVertical:10, marginLeft:15}}/>
          {/* <Text style={this.props.textHello}>Hello, <Text 
          style={this.props.textHello}>
              {this.props.title}
          </Text>
          </Text> */}
        </View>
      </View>
    );
  }
}
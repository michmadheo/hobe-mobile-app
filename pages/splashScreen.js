import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import { TEMA_UTAMA } from '../src/colors';
import { hobeLogoWhite } from '../src/images';
import { store } from '../functions/astore';
import userStore from '../stores/userStore';
import { fetchPOST } from '../functions/fetch';
import { autoLoginCheck } from '../api/api';

export default class Splashscreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      reptile: 'Welcome to Menu',
      color: '#008f68',
    };
  }

  componentDidMount(){
    // setTimeout(()=> this.loginCheck(), 3000);
    this.loginCheck()
  }

  async loginCheck(){
    let loginCredential = await store('get', 'loginCred');
    if(loginCredential !== null){
      let loginData = await JSON.parse(loginCredential)
      userStore.name = loginData.name
      userStore.username = loginData.username
      userStore.email = loginData.email
      userStore.phone = loginData.phone
      userStore.token = loginData.token
      userStore.id = loginData.id
      userStore.profilePhoto = loginData.profilePhoto
      let request = {
        username: loginData.username,
        token: loginData.token
      }
      fetchPOST(autoLoginCheck, request).then((response)=>{
        if(response.status === "SUCCESS"){
          this.props.navigation.replace('Dashboard')
          userStore.name = response.data.name
          userStore.username = response.data.username
          userStore.email = response.data.email
          userStore.phone = response.data.phone
          userStore.token = response.data.token
          userStore.id = response.data.id
          userStore.profilePhoto = response.data.profilePhoto
        }
        else{
          alert(response.message)
              store('del','loginCred');
              userStore.name = ""
              userStore.username = ""
              userStore.email = ""
              userStore.phone = ""
              userStore.token = ""
              userStore.profilePhoto = ""
          this.props.navigation.replace('Main')
        }
      })
    }
    else{
      this.props.navigation.replace('Main')
    }
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor:TEMA_UTAMA}}>
        {/* <Text>{this.state.reptile}</Text> */}
        <Image source={hobeLogoWhite} style={{resizeMode:'contain', width:200}}/>
      </View>
    );
  }
}
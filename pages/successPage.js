import React, { Component } from 'react';
import { Text, View, TouchableOpacity,} from 'react-native';
import firebase from 'firebase';
import userStore from '../stores/userStore';

import * as COLOR from '../src/colors';
import { Colors } from 'react-native/Libraries/NewAppScreen';

import HeaderBack from '../component/headerBack';

import LottieView from 'lottie-react-native';
import { check_Anim, paperPlane_Anim } from '../src/images';


export default class SuccessPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
        email: "",
        password: "",
        logged: null,
        goBackViaButton: false,
        imageChange:false,
    };
  }

  componentDidMount(){
      setTimeout(()=> this.state.goBackViaButton?null:this.directing(), 10000);
    //   alert('Yuhu')
    // this.loginCheck()
  }

  goTo = () => {this.props.navigation.navigate(this.props.route.params.goTo)}

  directing(){
    this.setState({
      imageChange:true
    })
    setTimeout(()=> this.state.goBackViaButton?null:this.goTo(), 3000);
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor:COLOR.PUTIH}}>
        <HeaderBack borderless={true}
        actionButton={()=>{
          this.setState({
            goBackViaButton:true
          })
          this.props.navigation.replace('Main')
          }}/>
          <View style={{flex:1, alignItems:'center', paddingTop:50}}>
            {this.state.imageChange?
              <LottieView source={paperPlane_Anim} style={{height:250, width:250}} autoPlay loop={true}/>:<LottieView source={check_Anim} style={{height:150, width:150}} autoPlay loop={false}/>

            }
            {this.state.imageChange?
            <Text style={{
              fontSize:15, 
              color:COLOR.HITAM_REDUP, 
              fontFamily:"sans-serif-light",
              marginTop:-50,
              paddingHorizontal:50}}>Automatically redirecting...</Text>:
              <View>
                <Text style={{
                fontSize:25, 
                color:COLOR.HITAM, 
                fontFamily:"sans-serif-light",
                paddingHorizontal:50,
                alignSelf:'center', 
                fontWeight:'bold'}}>Success</Text>
                <Text style={{
                fontSize:15, 
                color:COLOR.HITAM_REDUP, 
                fontFamily:"sans-serif-light",
                alignSelf:'center',
                paddingHorizontal:20}}>{this.props.route.params.message}</Text>
                </View>}
            {/* <Text style={{
                  fontSize:20, 
                  color:COLOR.HITAM_REDUP, 
                  fontFamily:"sans-serif-light",
                  paddingHorizontal:50, 
                  fontWeight:'bold'}}>{this.props.route.params.message}</Text> */}
          </View>
          <View style={{paddingBottom:20}}>
            <TouchableOpacity onPress={()=>{
              this.setState({
                goBackViaButton:true
              })
              this.props.navigation.navigate(this.props.route.params.goTo)
            }} 
            style={{
              backgroundColor:COLOR.TEMA_UTAMA, 
              justifyContent:'center', 
              alignItems:'center', 
              marginHorizontal:20, 
              borderRadius:10}}>
            <Text style={{
                marginVertical:8, 
                fontSize:20, 
                color:COLOR.PUTIH}}>
                  Okay
                  </Text>
            </TouchableOpacity>
          </View>
      </View>
    );
  }
}

//Styling

const formContainer={
  backgroundColor:COLOR.PUTIH,
  borderWidth:2,
  borderColor:COLOR.ORANYE,
  borderRadius:100,
  marginHorizontal:20,
  marginBottom:20
}

const buttonContainer={
  backgroundColor:COLOR.ORANYE,
  borderRadius:100,
  marginHorizontal:20,
  marginBottom:20
}

const formText={
  fontSize:25,
  marginVertical:5,
  marginLeft:20
}

const buttonText={
  fontSize:20,
  marginVertical:15,
  alignSelf:'center',
  color:COLOR.PUTIH
}
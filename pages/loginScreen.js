import React, { Component } from 'react';
import { Text, View, TextInput, TouchableOpacity, TouchableWithoutFeedback, Image, ActivityIndicator } from 'react-native';
import firebase from 'firebase';
import {initializeFirebase, signOut} from '../functions/firebaseFunctions/firebaseFunctions';
import userStore from '../stores/userStore';

import * as COLOR from '../src/colors';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { fetchPOST } from '../functions/fetch';
import { store } from '../functions/astore';
import { login } from '../api/api';

import HeaderBack from '../component/headerBack';

import { hobeLogo, invisPass, visiblePass } from '../src/images';
import LoadingScreen from '../component/loadingScreen';

// initializeFirebase()
export default class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
        username: "",
        password: "",
        errLogin: "",
        logged: null,
        passVisible: true,
        loginButton:false,
        loading: false
    };
  }

  componentDidMount(){
    // this.loginCheck()
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor:COLOR.PUTIH}}>
        <HeaderBack borderless={true} actionButton={()=>{
          this.setState({
            goBackViaButton:true
          })
          this.props.navigation.replace('Main')
          }}/>
        <View style={{justifyContent:'center',marginHorizontal:25, flex:1}}>
          <View style={{alignItems:'center'}}>
          <Image source={hobeLogo} style={{resizeMode:'contain', height:50, marginBottom:30}}/>
          {/* <Text style={{
            fontSize:30, 
            color:COLOR.HITAM_REDUP, 
            fontFamily:"sans-serif-medium",
            marginBottom:25}}>Welcome to Hobé</Text> */}
          </View>
          <View style={formContainer}>
            <TextInput
                secureTextEntry={false}
                placeholder={"Username or Email"}
                style={formText}
                onChangeText={(username) => this.setState({
                  username,
                  errLogin: ""
                })}
                value={this.state.username}/>
          </View>
          <View style={formContainer}>
            <TextInput
                secureTextEntry={this.state.passVisible}
                placeholder={"Password"}
                style={formText}
                onChangeText={(password) => this.setState({
                  password,
                  errLogin: ""
                })}
                value={this.state.password}/>
                {this.state.password.length>0?
                <TouchableWithoutFeedback 
                onPress={()=>{
                  this.setState({
                    passVisible:!this.state.passVisible
                  })
                }}
                >
                  <Image source={this.state.passVisible?invisPass:visiblePass} style={{width:30, height:30, marginRight:20, alignSelf:'center'}}/>
                </TouchableWithoutFeedback>:null}

          </View>
          {!this.state.loginButton?
          <TouchableOpacity 
          onPress={()=>{
            if(this.validate()){
              this.login()
            }
            // this.props.navigation.replace('Dashboard')
          }} 
          style={{
            backgroundColor:COLOR.TEMA_UTAMA,
            borderRadius:10,
            marginBottom:10,
            justifyContent:'center', 
            alignItems:'center'}}>
              <Text style={{
                marginVertical:8, 
                fontSize:20, 
                color:COLOR.PUTIH,
                fontFamily:"sans-serif-light"}}>
                  Login
                  </Text>
          </TouchableOpacity>:<ActivityIndicator size="large" color={COLOR.TEMA_UTAMA} />
          }
              <Text style={errText}>
                {this.state.errLogin}
              </Text>
        </View>
        <TouchableOpacity 
          onPress={()=>{
            this.props.navigation.navigate('Register')
            }} 
            style={{
              alignItems:'center', 
              marginBottom:15}}>
              <Text style={{
                fontSize:15, 
                fontFamily:"sans-serif-light"}}>
                  Don't have an account? <Text style={{
                    color:COLOR.TEMA_UTAMA}}>create an account</Text>
                      </Text>
          </TouchableOpacity>
          <LoadingScreen load={this.state.loading}/>
      </View>
    );
  }

  validate(){
    let login = true
    if(this.state.username === "" || this.state.password === ""){
      this.setState({
        errLogin: "We need your username and password to continue"
      })
      login = false
    }
    return login
  }

  login(){
    this.setState({
      loginButton: true,
      loading:true
    })
    let request = {
      username:this.state.username,
      password: this.state.password
    }
    fetchPOST(login, request).then(async(response)=>{
      if(response.status === "SUCCESS"){
        userStore.name = await response.data.name,
        userStore.username =  await response.data.username,
        userStore.email = await response.data.email,
        userStore.phone = await response.data.phone,
        userStore.token = await response.data.token,
        userStore.id = await response.data.id,
        userStore.profilePhoto = await response.data.profilePhoto
        store('set', 'loginCred', JSON.stringify(userStore));
        this.setState({
          loading:false
        })
        this.props.navigation.replace('Dashboard')
      }
      else{
        this.setState({
          errLogin: response.message,
          loginButton:false,
          loading: false
        })
      }
    })
  }
}

//Styling

const formContainer={
  backgroundColor:COLOR.ABU_TERANG,
  borderColor:COLOR.TEMA_UTAMA,
  borderRadius:10,
  marginBottom:10,
  flexDirection:'row'
}

const buttonContainer={
  backgroundColor:COLOR.TEMA_UTAMA,
  borderRadius:100,
  marginHorizontal:20,
  marginBottom:20
}

const formText={
  fontSize:20,
  marginVertical:5,
  marginLeft:20,
  flex:1
}

const buttonText={
  fontSize:20,
  marginVertical:15,
  alignSelf:'center',
  color:COLOR.PUTIH
}

const errText={
  color:COLOR.MERAH,
  paddingBottom:5
}
import React, { Component } from 'react';
import { Text, View, TouchableOpacity,} from 'react-native';
import userStore from '../stores/userStore';

import * as COLOR from '../src/colors';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import LottieView from 'lottie-react-native';
import { check, gardening_Anim, paperPlane_Anim, skating_Anim, cooking_Anim, watching_Anim } from '../src/images';
import WelcomeAnimation from '../component/welcomeAnimation';
import AsynchStorage from '@react-native-community/async-storage';
import {store} from '../functions/astore';

export default class Welcome extends Component {

  constructor(props) {
    super(props);
    this.state = {
        email: "",
        password: "",
        logged: null,
    };
  }

  componentDidMount(){
    // this.loginCheck()
  }

  // async loginCheck(){
  //   let loginCredential = await store('get', 'loginCred');
  //   if(loginCredential !== null){
  //     let loginData = await JSON.parse(loginCredential)
  //     userStore.name = loginData.name
  //     userStore.username = loginData.username
  //     userStore.email = loginData.email
  //     userStore.phone = loginData.phone
  //     userStore.token = loginData.token
  //     this.props.navigation.replace('Dashboard')
  //   }
  // }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor:COLOR.PUTIH}}>
          <View style={{flex:1,marginLeft:30, marginTop:50}}>
              <Text style={{
                fontSize:50, 
                color:COLOR.HITAM_REDUP, 
                fontFamily:"sans-serif-light", 
                fontWeight:'bold'}}>
                  Hello there!
                </Text>
              <Text style={{
                fontSize:30, 
                color:COLOR.HITAM_REDUP, 
                fontFamily:"sans-serif-thin"}}>
                  Let's get started!
                  </Text>
              <TouchableOpacity onPress={()=>{this.props.navigation.replace('Login')}} style={{
                marginTop:40, 
                backgroundColor:COLOR.TEMA_UTAMA, 
                width:150, borderRadius:100, 
                justifyContent:'center', 
                alignItems:'center'}}>
                  <Text style={{
                    marginVertical:8, 
                    fontSize:25, 
                    color:COLOR.PUTIH,
                    fontFamily:"sans-serif-light"}}>
                      Login
                      </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Register')}} style={{
                borderWidth:2, 
                borderColor:COLOR.TEMA_UTAMA,
                marginTop:10, 
                backgroundColor:COLOR.PUTIH, 
                width:150, 
                borderRadius:100, 
                justifyContent:'center', 
                alignItems:'center'}}>
                  <Text style={{
                    marginVertical:8, 
                    fontSize:25, 
                    color:COLOR.HITAM_REDUP, 
                    fontFamily:"sans-serif-light"}}>
                      Sign-up
                      </Text>
              </TouchableOpacity>
              {/* <LottieView style={{height:250, alignSelf:'center'}} source={gardening_Anim} autoPlay loop/> */}
              <WelcomeAnimation/>
          </View>
          <TouchableOpacity 
          onPress={()=>{
            alert('Hublah!')
            }} 
            style={{
              alignItems:'center', 
              marginBottom:15}}>
              <Text style={{
                fontSize:15, 
                fontFamily:"sans-serif-light"}}>
                  Or maybe, let us tell you more about <Text style={{
                    color:COLOR.TEMA_UTAMA}}>Hobé</Text>
                      </Text>
          </TouchableOpacity>
      </View>
    );
  }
}

//Styling

const formContainer={
  backgroundColor:COLOR.PUTIH,
  borderWidth:2,
  borderColor:COLOR.ORANYE,
  borderRadius:100,
  marginHorizontal:20,
  marginBottom:20
}

const buttonContainer={
  backgroundColor:COLOR.ORANYE,
  borderRadius:100,
  marginHorizontal:20,
  marginBottom:20
}

const formText={
  fontSize:25,
  marginVertical:5,
  marginLeft:20
}

const buttonText={
  fontSize:20,
  marginVertical:15,
  alignSelf:'center',
  color:COLOR.PUTIH
}
import React, { Component, useState } from 'react';
import { Text, View, TouchableOpacity, TextInput, Modal, Image, FlatList, ImageBackground } from 'react-native';
import {store} from '../functions/astore';
import {signOut} from '../functions/userFunction';
import HeaderBackButton from '../component/headerBackButton';
import * as COLOR from '../src/colors';
import userStore from '../stores/userStore';
import {dropDown} from '../src/images';
import { fetchPOST } from '../functions/fetch';
import { getAllTopic, postNew, editPost } from '../api/api';
import { ScrollView } from 'react-native-gesture-handler';
import LoadingScreen from '../component/loadingScreen';

export default class EditPost extends Component {

  static defaultProps ={
    singlePage : false
  }

  constructor(props) {
    super(props);
    this.state = {
      topicModal:false,
      topicList:[],
      myTopic:[],
      title:this.props.route.params.title,
      content:this.props.route.params.content,
      postId: this.props.route.params.postId,
      topicTitle:"",
      topicId:"",
      topicsMenu: 'liked',
      loading:false,
    };
  }

  componentDidMount(){
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor:'white'}}>
          <HeaderBackButton 
          title={"Edit Post "} 
          actionButton={()=>{this.props.navigation.goBack()}} 
          secondActionButton={()=>{
            if(this.validatePost()){
              this.post()
              }}}/>
          <View style={{marginHorizontal:10, paddingTop:10}}>
            <View style={{flexDirection:'row'}}>
              <View style={{height:40, width:40, backgroundColor:COLOR.ABU_INPUT, borderRadius:50, justifyContent:'center'}}>
                {userStore.profilePhoto?
                  <ImageBackground source={{uri: 'data:image/png;base64,'+userStore.profilePhoto}} imageStyle={{borderRadius:50}} style={{height:40, width:40, resizeMode:'cover', justifyContent:'center'}}/>:
                  <Text style={{alignSelf:'center', fontSize:20, color:COLOR.PUTIH}}>{userStore.name.charAt(0)}</Text>
                }
              </View>
              <Text style={{justifyContent:'center', alignSelf:'center', paddingLeft:10, color:COLOR.HITAM_REDUP}}>Edit your post</Text>
            </View>
            <TextInput
                  placeholder="eg: Apa genre musik terbaik...Mana yang merupakan..."
                  autoCorrect={false} 
                  autoCompleteType={'off'}
                  multiline={true}
                  onChangeText={(title)=>{
                    this.setState({
                    title})
                  }
                  }
                  style={{color:COLOR.HITAM_REDUP, fontSize:17, fontWeight:'bold'}}
                  value={this.state.title}
                />
            <View style={{height:2, backgroundColor:COLOR.ABU_INPUT}}/>
            <TextInput
                  placeholder="eg: Saya sangat mencintai musik, apa genre musik terbaik yang..."
                  autoCorrect={false} 
                  autoCompleteType={'off'}
                  multiline={true}
                  onChangeText={(content)=>{
                    this.setState({
                    content})
                  }
                  }
                  style={{color:COLOR.HITAM_REDUP, fontSize:15,paddingBottom:200}}
                  value={this.state.content}
                />
          </View>
        <Modal
        animationType="slide"
        transparent={true}
        onRequestClose={()=>{
          this.setState({
            topicModal:false
          })
        }}
        visible={this.state.topicModal}
        >
          <TouchableOpacity onPress={()=>{this.setState({
            topicModal:false
          })}} style={{flex:1, backgroundColor:COLOR.HITAM_REDUP, opacity:0.2}}>
          </TouchableOpacity>
          <View style={{flex:1, backgroundColor:COLOR.PUTIH}}>
            <View style={{flexDirection:'row'}}>
              <TouchableOpacity 
              onPress={()=>{this.setState({
                topicsMenu:'liked'
              })}} 
              style={{
                flex:1, 
                paddingVertical:20}}>
                <Text style={{alignSelf:'center', paddingBottom:10, color:COLOR.HITAM_REDUP}}>Liked Topics</Text>
                <View style={{height:2, backgroundColor:this.state.topicsMenu === 'liked'?COLOR.TEMA_UTAMA:COLOR.PUTIH}}></View>
              </TouchableOpacity>
              <TouchableOpacity 
              onPress={()=>{this.setState({
                topicsMenu:'all'
              })}} 
              style={{
                flex:1, 
                paddingVertical:20}}>
              <Text style={{alignSelf:'center', paddingBottom:10, color:COLOR.HITAM_REDUP}}>All Topics</Text>
                <View style={{height:2, backgroundColor:this.state.topicsMenu === 'all'?COLOR.TEMA_UTAMA:COLOR.PUTIH}}></View>
              </TouchableOpacity>
            </View>
            {this.state.topicsMenu === 'all'?
            <View style={{flex:1}}>
              <ScrollView>
              {this.state.topicList.map((item, index)=>{
                return(
                  <TouchableOpacity
                  key={index} 
                  style={{
                    paddingVertical:8,
                    paddingHorizontal:20
                  }}
                  onPress={()=>{
                    this.setState({
                      topicTitle:item.title,
                      topicId:item._id,
                      topicModal:false
                    })
                  }}>
                    <Text style={{fontSize:15, marginBottom:10, color:COLOR.HITAM_REDUP}}>{item.title}</Text>
                    <View style={{height:2, backgroundColor:COLOR.ABU_TERANG}}></View>
                  </TouchableOpacity>
                )
              })}
              </ScrollView>
            </View>:null
            }
          </View>
        </Modal>
        <LoadingScreen load={this.state.loading}/>
      </View>
    );
  }

  validatePost(){
    let validate = true
    if(this.state.title === ""){
      validate = false
    }
    if(this.state.content === ""){
      validate = false
    }
    return validate
  }

  getTopics(){
    let request = {
      username:userStore.username
    }
    fetchPOST(getAllTopic, request).then((response)=>{
      if(response.status === "SUCCESS"){
        this.setState({
          topicList:response.data,
          loading: false,
        })
        // alert(JSON.stringify(response.data[0]._id))
      }
    })
  }

  post(){
    this.setState({loading:true})
    let request = {
      username:userStore.username,
      title:this.state.title,
      content: this.state.content,
      post_id: this.state.postId,
    }
    fetchPOST(editPost, request).then((response)=>{
      if(response.status === "SUCCESS"){
        this.setState({loading:false})
        // alert(response.data)
        if(this.props.route.params.singlePage){
          this.props.navigation.goBack()
        }
        else{
          this.props.navigation.replace('Dashboard', {initialRoute:"Activity"})
        }
      }
      else{
        this.setState({loading:false})
        alert(response.message)
      }
    })
  }
}

const containerBox={
  borderRadius:5,
  paddingLeft:10, 
  paddingVertical:30,
  marginBottom:10,
  backgroundColor:COLOR.PUTIH, 
  shadowColor: "#000",
  shadowOffset: {
  width: 0,
  height: 1,
  },
  shadowOpacity: 0.20,
  shadowRadius: 1.41,
  elevation: 2,}
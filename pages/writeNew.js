import React, { Component, useState } from 'react';
import { Text, View, TouchableOpacity, TextInput, Modal, Image, FlatList, ImageBackground } from 'react-native';
import {store} from '../functions/astore';
import {signOut} from '../functions/userFunction';
import HeaderBackButton from '../component/headerBackButton';
import * as COLOR from '../src/colors';
import userStore from '../stores/userStore';
import {addPhoto, cancel, dropDown} from '../src/images';
import { fetchPOST } from '../functions/fetch';
import { getAllTopic, getProfilePhoto, postNew } from '../api/api';
import { ScrollView } from 'react-native-gesture-handler';
import LoadingScreen from '../component/loadingScreen';
import { serverAPI } from '../server-configuration/server';
import ImagePicker from 'react-native-image-picker';

export default class WriteNew extends Component {

  constructor(props) {
    super(props);
    this.state = {
      topicModal:false,
      topicList:[],
      myTopic:[],
      title:"",
      content:"",
      // topicTitle:"Art",
      // topicId:"5f6f6aced114aba186fc9837",
      topicTitle:"",
      topicId:"",
      topicsMenu: 'liked',
      loading:true,
      image:"",
      imageHeight:"",
      imageWidth:"",
      focusFlex:1
    };
  }

  componentDidMount(){
    this.getTopics()
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor:'white'}}>
          <HeaderBackButton 
          title={"New Post"} 
          actionButton={()=>{this.props.navigation.goBack()}} 
          secondActionButton={()=>{
            if(this.validatePost()){
              this.post()
              }}}/>
          <View style={{marginHorizontal:10, paddingTop:10, flex:1}}>
            {/* <Text style={{fontWeight:'bold', fontSize:15, paddingBottom:10, color:COLOR.HITAM_REDUP}}>Let's start a talk, {userStore.name}</Text> */}
            <View style={{flexDirection:'row'}}>
              <View style={{height:40, width:40, backgroundColor:COLOR.ABU_INPUT, borderRadius:50, justifyContent:'center'}}>
                {userStore.profilePhoto?
                  <ImageBackground source={{uri: serverAPI+getProfilePhoto+userStore.profilePhoto}} imageStyle={{borderRadius:50}} style={{height:40, width:40, resizeMode:'cover', justifyContent:'center'}}/>:
                  <Text style={{alignSelf:'center', fontSize:20, color:COLOR.PUTIH}}>{userStore.name.charAt(0)}</Text>
                }
              </View>
              <Text style={{justifyContent:'center', alignSelf:'center', paddingLeft:10, color:COLOR.HITAM_REDUP}}>Let's talk about </Text>
              <TouchableOpacity 
              style={{justifyContent:'center'}}
              onPress={()=>{this.setState({
                topicModal:true,
              })}}
              >
                <View style={{flexDirection:'row'}}>
                  {this.state.topicTitle !== ""?
                    <Text>{this.state.topicTitle}</Text>:
                    <Text style={{opacity:0.3}}>Choose topic</Text>
                  }
                  {/* <Text>Choose topic</Text> */}
                    <View style={{justifyContent:'center', paddingLeft:5}}>
                      <Image source={dropDown} style={{width:10, height:10}}/>
                    </View>
                </View>
              </TouchableOpacity>
            </View>
            {/* <Text style={{fontWeight:'bold', paddingTop:10, fontSize:18}}>Title</Text> */}
            <TextInput
                  placeholder="eg: Apa genre musik terbaik...Mana yang merupakan..."
                  autoCorrect={false} 
                  autoCompleteType={'off'}
                  multiline={true} 
                  onFocus={()=>{

                  }}
                  onChangeText={(title)=>{
                    this.setState({
                    title})
                  }
                  }
                  style={{color:COLOR.HITAM_REDUP, fontSize:17, fontWeight:'bold'}}
                  value={this.state.title}
                />
            <View style={{height:2, backgroundColor:COLOR.ABU_INPUT}}/>
            {/* <Text style={{fontWeight:'bold', paddingTop:10, fontSize:18}}>Title</Text> */}
            <TextInput
                  placeholder="eg: Saya sangat mencintai musik, apa genre musik terbaik yang..."
                  autoCorrect={false} 
                  autoCompleteType={'off'}
                  multiline={true}
                  onChangeText={(content)=>{
                    this.setState({
                    content})
                  }
                  }
                  style={{color:COLOR.HITAM_REDUP, fontSize:15, paddingBottom:100}}
                  value={this.state.content}
                />
          </View>
          <View style={{marginHorizontal:10, paddingTop:10, flex:1}}>
            <TouchableOpacity onPress={()=>{
              this.choosePhoto()
            }}>
              {this.state.image === ""?
                <Image source={addPhoto} style={{height:80, width:80}}/>:
                <View style={{flexDirection:'row'}}>
                  <Image source={{uri: 'data:image/png;base64,'+this.state.image}} style={{resizeMode:'cover', height:200,width:200}}/>
                  <TouchableOpacity onPress={()=>{
                    this.setState({
                      image:""
                    })
                  }} style={{marginLeft:10}}>
                    <Image source={cancel} style={{height:20, width:20}}/>
                  </TouchableOpacity>
                </View>
              }
            </TouchableOpacity>
          </View>
        <Modal
        animationType="slide"
        transparent={true}
        onRequestClose={()=>{
          this.setState({
            topicModal:false
          })
        }}
        visible={this.state.topicModal}
        >
          <TouchableOpacity onPress={()=>{this.setState({
            topicModal:false
          })}} style={{flex:1, backgroundColor:COLOR.HITAM_REDUP, opacity:0.2}}>
          </TouchableOpacity>
          <View style={{flex:1, backgroundColor:COLOR.PUTIH}}>
            <View style={{flexDirection:'row'}}>
              <TouchableOpacity 
              onPress={()=>{this.setState({
                topicsMenu:'liked'
              })}} 
              style={{
                flex:1, 
                paddingVertical:20}}>
                <Text style={{alignSelf:'center', paddingBottom:10, color:COLOR.HITAM_REDUP}}>Liked Topics</Text>
                <View style={{height:2, backgroundColor:this.state.topicsMenu === 'liked'?COLOR.TEMA_UTAMA:COLOR.PUTIH}}></View>
              </TouchableOpacity>
              <TouchableOpacity 
              onPress={()=>{this.setState({
                topicsMenu:'all'
              })}} 
              style={{
                flex:1, 
                paddingVertical:20}}>
              <Text style={{alignSelf:'center', paddingBottom:10, color:COLOR.HITAM_REDUP}}>All Topics</Text>
                <View style={{height:2, backgroundColor:this.state.topicsMenu === 'all'?COLOR.TEMA_UTAMA:COLOR.PUTIH}}></View>
              </TouchableOpacity>
            </View>
            {this.state.topicsMenu === 'all'?
            <View style={{flex:1}}>
              <ScrollView>
              {this.state.topicList.map((item, index)=>{
                return(
                  <TouchableOpacity
                  key={index} 
                  style={{
                    paddingVertical:8,
                    paddingHorizontal:20
                  }}
                  onPress={()=>{
                    this.setState({
                      topicTitle:item.title,
                      topicId:item._id,
                      topicModal:false
                    })
                  }}>
                    <Text style={{fontSize:15, marginBottom:10, color:COLOR.HITAM_REDUP}}>{item.title}</Text>
                    <View style={{height:2, backgroundColor:COLOR.ABU_TERANG}}></View>
                  </TouchableOpacity>
                )
              })}
              </ScrollView>
            </View>:null
            }
          </View>
        </Modal>
        <LoadingScreen load={this.state.loading}/>
      </View>
    );
  }

  async choosePhoto(){
    const options = {
      multiple: false,
      quality: 1,
    };
    ImagePicker.launchImageLibrary(options, response =>{
      let imageFile = response.data
      console.log("response", response)
      if(response.data){
        if(response.fileSize > 1500000){
          this.setState({loading:false})
          alert('Max. file size for Image is 1.5 Mb')
        }
        else{
          this.setState({
            image: response.data,
            imageHeight: response.height,
            imageWidth: response.width
          })
        }
      }
    })
}

  validatePost(){
    let validate = true
    if(this.state.title === ""){
      validate = false
    }
    if(this.state.content === ""){
      validate = false
    }
    if(this.state.topicId === ""){
      validate = false
    }
    return validate
  }

  getTopics(){
    let request = {
      username:userStore.username
    }
    fetchPOST(getAllTopic, request).then((response)=>{
      if(response.status === "SUCCESS"){
        this.setState({
          topicList:response.data,
          loading: false,
        })
        // alert(JSON.stringify(response.data[0]._id))
      }
    })
  }

  post(){
    this.setState({loading:true})
    let request = {
      username:userStore.username,
      title:this.state.title,
      content: this.state.content,
      topic:this.state.topicId,
      image:this.state.image
    }
    fetchPOST(postNew, request).then((response)=>{
      if(response.status === "SUCCESS"){
        this.setState({loading:false})
        this.props.navigation.replace('Dashboard', {initialRoute:"Activity"})
      }
      else{
        this.setState({loading:false})
        alert(response.message)
      }
    })
  }
}

const containerBox={
  borderRadius:5,
  paddingLeft:10, 
  paddingVertical:30,
  marginBottom:10,
  backgroundColor:COLOR.PUTIH, 
  shadowColor: "#000",
  shadowOffset: {
  width: 0,
  height: 1,
  },
  shadowOpacity: 0.20,
  shadowRadius: 1.41,
  elevation: 2,}
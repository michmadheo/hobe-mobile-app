import React, { Component } from 'react';
import { Text, View, Button, TouchableOpacity, ColorPropType, BackHandler, Image, ScrollView, Animated } from 'react-native';
import Header from '../component/header';
import * as COLOR from '../src/colors';

import {fetchPOST} from '../functions/fetch';
import {HERO_LIST, demoAPI, appdescription} from '../api/api';

import demoStore from '../stores/demoStore';

import {signOut} from '../functions/firebaseFunctions/firebaseFunctions';
import userStore from '../stores/userStore';

import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { search } from '../src/images';

const Tab = createBottomTabNavigator();

export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      heroName: "Goblok",
      topics: [],
      appName: "",
      appDesc: "",
      // searchBarPos: new Animated.Value(-50),
    };
  }

  moveSearchBar = () =>{
    Animated.spring(this.state.searchBarPos, {
        toValue:0,
        useNativeDriver: true,
        bounciness:12,
    }).start()
  }

  componentDidMount(){
    // this.focusListener = this.props.navigation.addListener('didFocus', () => {
    //   this.moveSearchBar()
    // });
  }

  // componentWillUnmount(){
  //   this.focusListener.remove()
  // }

  render() {
    return (
      <View style={{flex:1, backgroundColor:COLOR.PUTIH}}>
        <Header 
        // title={userStore.data.displayName}
        // title={userStore.name}
        />
        <View style={{marginHorizontal:20}}>
          {/* <Text style={{color:COLOR.HITAM_REDUP, fontSize:20}}>Hello, {userStore.name}</Text> */}
          <Animated.View>
          <TouchableOpacity 
          style={{height:50, alignItems:'center', marginTop:10, backgroundColor:COLOR.ABU_TERANG, borderRadius:50, flexDirection:'row'}}
          onPress={()=>{this.props.navigation.navigate('SearchPage')}}
          >
            <Image source={search} style={{width:20, height:20, marginLeft:20, opacity:0.7}}/>
            <Text style={{paddingLeft:10, color:COLOR.HITAM_REDUP, opacity:0.5, fontSize:15}}>Let's start discovering...</Text>
          </TouchableOpacity>
          </Animated.View>
          <ScrollView style={{marginTop:20}}>
            <View style={{flexDirection:'row'}}>
              <View style={{flex:1}}>
                <Text style={{fontSize:18, fontWeight:'bold'}}>Topics</Text>
                {/* <Text style={{fontSize:13}}>Topics you might find interesting</Text> */}
              </View>
              <TouchableOpacity onPress={()=>{alert('wow')}}>
                <Text style={{fontSize:15, alignSelf:'flex-end', color:COLOR.TEMA_UTAMA}}>View all</Text>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row', paddingTop:10}}>
              {/* <View style={{width:300, height:150}}>
                <View style={{flex:1, backgroundColor:'blue', borderTopStartRadius:20, borderTopEndRadius:20}}></View>
                <View style={{flex:1, backgroundColor:'red', borderBottomStartRadius:20, borderBottomEndRadius:20}}></View>
              </View> */}
              {/* <View style={{width:70, height:70, borderRadius:100, backgroundColor:'green'}}></View> */}
            </View>
          </ScrollView>
        </View>
        <Text style={{paddingHorizontal:15, fontSize:20}}>{this.state.appDesc}</Text>
      </View>
    );
  }

  async getDemoAPI(){
    let request = {name:"wedew"}
    fetchPOST(appdescription, request).then((response)=>{
      let data = response
      // alert(JSON.stringify(response))
      this.setState({
        appName:response.message,
        appDesc:response.desc
      })
    })
  }

  async getTopics(){

  }
}

const navigationBar = {
  backgroundColor:COLOR.PUTIH, 
  height:60, width:'100%', 
  justifyContent:'center',
  shadowColor: "#000",
  shadowOffset: {                 
    width: 0,                
    height: 4,    
    },           
  shadowOpacity: 0.32,      
  shadowRadius: 5.46,
  elevation: 9,
}
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, FlatList, Image, ImageBackground, ScrollView, ActivityIndicator } from 'react-native';
import { ColorAndroid } from 'react-native/Libraries/StyleSheet/PlatformColorValueTypesAndroid';
import { getPost, deletePost, getProfilePhoto, getImage } from '../api/api';
import {store} from '../functions/astore';
import { fetchPOST } from '../functions/fetch';
import {signOut} from '../functions/userFunction';
import userStore from '../stores/userStore';
import * as COLOR from '../src/colors';
import { formatDateForActivity, randomColor } from '../functions/generalFunctions';
import { comment, gardening_Anim, heart, heartFilled, more, paperPlane_Anim } from '../src/images';
import LottieView from 'lottie-react-native';
import ActivityActionModal from '../component/activityActionModal';
import ActivtyActionModal from '../component/activityActionModal';
import ConfirmationModal from '../component/confirmationModal';
import LoadingScreen from '../component/loadingScreen';
import { serverAPI } from '../server-configuration/server';
import ImageViewer from '../component/imageViewer';

export default class Activity extends Component {

  constructor(props) {
    super(props);
    this.state = {
        data:[],
        refresh:false,
        love : false,
        contentLove: [],
        page: 0,
        postActionModal:false,
        confirmationModal:false,
        yourPost: false,
        currTitle: "",
        currContent: "",
        currID: "",
        loading:false,
        firstData:0,
        loadNext: false,
        viewImage: false,
        viewedImage: "",
        maximumPosts: false,
    };
  }

  componentDidMount(){
      this.getPosts()
  }

  refreshPost(){
      this.setState({
          data:[],
          refresh: true,
          page: 0,
          firstData: 0,
          maximumPosts: false,
          loadNext:false,
      },()=>{this.getPosts()})
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{backgroundColor:COLOR.PUTIH, paddingHorizontal:20, paddingVertical:10, flexDirection:'row'}}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <TouchableOpacity style={{backgroundColor:COLOR.TEMA_UTAMA, borderRadius:50, marginRight:5}}>
              <Text style={{fontSize:15, color:COLOR.PUTIH, paddingHorizontal:10, paddingVertical:5}}>All Topics</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{backgroundColor:COLOR.ABU_INPUT, borderRadius:50, marginRight:5}}>
              <Text style={{fontSize:15, color:COLOR.PUTIH, paddingHorizontal:10, paddingVertical:5}}>Coming soon</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{backgroundColor:COLOR.ABU_INPUT, borderRadius:50, marginRight:5}}>
              <Text style={{fontSize:15, color:COLOR.PUTIH, paddingHorizontal:10, paddingVertical:5}}>Coming Soon</Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
        <FlatList
            data={this.state.data}
            showsVerticalScrollIndicator={false}
            onRefresh={()=>this.refreshPost()}
            refreshing={this.state.refresh}
            onEndReached={()=>{this.getPostsNextPage()}}
            renderItem={({item, index})=>{
              let love = false;
                return (
                    <View key={item._id} style={{backgroundColor:COLOR.PUTIH, marginBottom:10, shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 1,
                    },
                    shadowOpacity: 0.20,
                    shadowRadius: 1.41,
                    
                    elevation: 2,}}>
                        <View style={{paddingHorizontal:15, paddingTop:10, paddingBottom:10}}>
                          <View style={{flexDirection:'row'}}>
                            <View style={{flexDirection:'row', flex:1}}>
                              <View style={{height:40, width:40, backgroundColor:COLOR.ABU_INPUT, borderRadius:50, justifyContent:'center', marginTop:4}}>
                                {item.creator.profilePhoto?
                                  <ImageBackground source={{uri: serverAPI+getProfilePhoto+item.creator.profilePhoto}} imageStyle={{borderRadius:50}} style={{height:40, width:40, resizeMode:'cover', justifyContent:'center'}}/>:
                                  <Text style={{alignSelf:'center', fontSize:20, color:COLOR.PUTIH}}>{item.creator.username.toUpperCase().charAt(0)}</Text>
                                }
                              </View>
                              <View style={{marginLeft: 8, justifyContent:'center'}}>
                                <Text style={{color:COLOR.HITAM_REDUP, fontFamily:"sans-serif-medium", fontSize:15}}>{item.creator.username}</Text>
                                <Text style={{color:COLOR.HITAM_REDUP, fontFamily:"sans-serif-light", fontSize:15}}>{item.topic.title}</Text>
                              </View>
                            </View>
                            <View style={{justifyContent:'center'}}>
                              <TouchableOpacity onPress={()=>{
                                this.triggerPostModal(item.creator._id, item.title, item.content, item._id)
                              }}>
                                <Image source={more} style={{width:20 , height:20}}/>
                              </TouchableOpacity>
                            </View>
                          </View>
                          <View style={{marginTop:5}}>
                            <Text style={{color:COLOR.HITAM_REDUP, fontSize:15, fontWeight:'bold'}}>{item.title}</Text>
                            <Text style={{color:COLOR.HITAM_REDUP, fontSize:15}}>
                              {item.content.substring(0,160)}
                              {item.content.length>160?"...":""}
                            </Text>
                            {item.content.length > 160?
                            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('CommentPage', {id:item._id})}}>
                              <Text style={{color:COLOR.HITAM_REDUP, fontSize:15, fontWeight:'bold'}}>Read More</Text>
                            </TouchableOpacity>:null
                            }
                            <TouchableOpacity onPress={()=>{this.viewImage(item.image)}}>
                            {
                              item.image?
                              <Image source={{uri: serverAPI+getImage+item.image}} style={{resizeMode:'contain', height:300, marginTop:10}}/>:
                              null
                            }
                            </TouchableOpacity>
                            <Text style={{color:COLOR.HITAM_REDUP, opacity:0.8}}>{formatDateForActivity(item.createOn)}</Text>
                          </View>
                          <View>
                            <View style={{height:2, backgroundColor:COLOR.ABU_TERANG, marginVertical:10}}/>
                            <View style={{flexDirection:'row'}}>
                              <TouchableOpacity onPress={()=>{
                                this.controlLove(index)
                                }}>
                                <Image source={this.state.contentLove[index]?heartFilled:heart} style={{marginLeft:5, height:21, width:21}}/>
                              </TouchableOpacity>
                              <TouchableOpacity style={{flexDirection:'row'}} onPress={()=>{this.props.navigation.navigate('CommentPage', {id:item._id})}}>
                                <Image source={comment} style={{marginLeft:15, height:20, width:20}}/>
                                {item.comments.length > 0?
                                  <Text style={{fontSize:15, paddingLeft:5}}>{item.comments.length}</Text>:null
                                }
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>
                    </View>
                )
            }}
            keyExtractor={item => item.key}
            contentContainerStyle={{ flexGrow: 1 }}
            ListEmptyComponent={this.renderLoading()}
            ListFooterComponent={this.renderFooter()}
        />
        <ActivtyActionModal 
        load={this.state.postActionModal}
        close={()=>{this.setState({postActionModal:false})}}
        yourPost={this.state.yourPost}
        actionEdit={()=>{
          this.setState({
            postActionModal:false
          })
          this.props.navigation.navigate('EditPost', {
            title:this.state.currTitle, 
            content:this.state.currContent, 
            postId:this.state.currID})
        }}
        actionDelete={
          ()=>{
            this.setState({
              postActionModal:false,
              confirmationModal:true
            })
          }
        }
        actionReport={
          ()=>{
            alert('Reported!')
            this.setState({
              postActionModal:false
            })
          }
        }
        />
        <ConfirmationModal 
        load={this.state.confirmationModal}
        text={"Confirm Delete?"}
        actionCancel={()=>{
          this.setState({
            confirmationModal:false
          })
        }}
        close={()=>{
          this.setState({
            confirmationModal:false
          })
        }}
        actionAccept={()=>{
          this.deletePost()
        }}
        />
        <ImageViewer close={()=>{this.setState({viewImage:false})}} load={this.state.viewImage} image={this.state.viewedImage}/>
        <LoadingScreen load={this.state.loading}/>
      </View>
    );
  }

  viewImage(imageId){
    this.setState({
      viewImage:true,
      viewedImage:imageId
    })
  }

  triggerPostModal(creatorId, itemTitle, itemContent, itemId){
    this.setState({
      postActionModal:true
    })
    if(creatorId === userStore.id){
      this.setState({
        yourPost:true,
        currTitle: itemTitle,
        currContent: itemContent,
        currID: itemId
      })
    }
    else{
      this.setState({
        yourPost:false
      })
    }
  }

  renderLoading(){
    return(
      <View style={{flex:1, justifyContent:'center', alignItems:'center', backgroundColor:COLOR.PUTIH}}>
        <LottieView style={{height:250}} source={paperPlane_Anim} autoPlay loop/>
      </View>
    )
  }
  
  renderFooter(){
    if(this.state.data.length > 0 && this.state.maximumPosts === false){
      return(
        <ActivityIndicator size="large" color={COLOR.TEMA_UTAMA} />
      )
    }
  }

  controlLove(itemIndex){
    let array = this.state.contentLove
    array.splice(itemIndex,1,!array[itemIndex])
    this.setState({
      contentLove:array
    })
  }

  getPosts(){
    this.setState({
      contentLove: [],
      refresh:false
    })
    let request = {
        username:userStore.username,
        page:this.state.page, 
    }
    fetchPOST(getPost, request).then((response)=>{
        if(response.status === 'SUCCESS'){
            this.setState({
                data: response.data,
                refresh: false,
                firstData: response.data[0].createOn
            }, ()=>{
              for(i=0; i<this.state.data.length; i++){
                this.state.contentLove.push(false)
              }
            })
        }
        else{
          alert(response.message)
        }
    })
  }

  getPostsNextPage(){
    if(this.state.loadNext === false){
      this.setState({
        loadNext: true
      })
      let currPage = this.state.page
      let nextPage = currPage+1
      let request = {
          username:userStore.username,
          page:nextPage,
          firstData: this.state.firstData
      }
      fetchPOST(getPost, request).then((response)=>{
          if(response.status === 'SUCCESS'){
            let moreLove = []
            for(i=0; i<response.data.length; i++){
              moreLove.push(false)
            }
            this.setState({
                data: [...this.state.data,...response.data],
                contentLove: [...this.state.contentLove,...moreLove],
                page: nextPage,
                refresh: false,
                loadNext:false
            })
          }
          if(response.status === 'ERROR'){
            alert(response.message)
          }
          if(response.status === 'MAXIMUM'){
            this.setState({
              maximumPosts:true
            })
          }
      })
    }
  }

  deletePost(){
    this.setState({
      confirmationModal:false,
      loading: true
    })
    let request = {
      username:userStore.username,
      post_id: this.state.currID
    }
    fetchPOST(deletePost, request).then((response)=>{
      if(response.status === 'SUCCESS'){
        // alert(response.message)
        this.setState({
          loading:false
        },()=>{
          this.refreshPost()
        })
      }
      else{
        alert(response.message)
        this.setState({
          loading:false
        })
      }
    })
  }
}
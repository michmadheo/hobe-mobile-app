import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, ImageBackground} from 'react-native';
import {signOut} from '../functions/userFunction';
import userStore from '../stores/userStore';
import * as COLOR from '../src/colors';
import ImagePicker from 'react-native-image-picker';
import { edit } from '../src/images';
import { fetchPOST } from '../functions/fetch';
import { uploadProfilePhoto, getProfilePhoto } from '../api/api';
import ImageViewer from '../component/imageViewer';
import { convertToBase64, convertToByteArray } from '../functions/imageConvert';
import { store } from '../functions/astore';
import LoadingScreen from '../component/loadingScreen';
import {serverAPI} from '../server-configuration/server';

export default class ProfilePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      image:userStore.profilePhoto,
      viewImage:false,
      chosenImage:"",
      loading: false,
    };
  }

  componentDidMount(){
  }

  async choosePhoto(){
      const options = {
        multiple: false,
        quality: 1,
      };
      ImagePicker.launchImageLibrary(options, response =>{
        let imageFile = response.data
        console.log("response", response)
        if(response.data){
          this.setState({loading:true})
          // this.uploadProfilePhoto(imageFile)
          // alert(response.fileSize)
          if(response.fileSize > 700000){
            this.setState({loading:false})
            alert('Max. file size for Profile Image is 700 kb')
          }
          else{
          this.uploadProfilePhoto(imageFile)
          }
        }
      })
  }
  
  uploadProfilePhoto(image){
    let request ={
      username:userStore.username,
      photoData: image
    }
    fetchPOST(uploadProfilePhoto, request).then((response)=>{
      if(response.status === 'SUCCESS'){
        alert(response.message)
        this.setState({
          image: response.data,
          loading: false
        },()=>{
          userStore.profilePhoto = response.data
          store('set', 'loginCred', JSON.stringify(userStore));
        })
      }
      else{
        this.setState({loading:false})
        alert(response.message)
      }
    })
  }

  render() {
    return (
      <View style={{ flex: 1,backgroundColor:COLOR.PUTIH }}>
        <View>
          <View style={{backgroundColor:COLOR.ABU_INPUT, height:70}}/>
          <View style={{backgroundColor:COLOR.PUTIH, height:50}}/>
          <View style={{marginTop:10, position:'absolute', bottom:1, left:20, flexDirection:'row'}}>
            <TouchableOpacity onPress={()=>{this.viewImage()}} style={{borderWidth:3, borderColor:COLOR.PUTIH, borderRadius:100}}>
                <View style={{height:100, width:100, backgroundColor:COLOR.ABU_INPUT, borderRadius:100, justifyContent:'center'}}>
                  {this.state.image === undefined?
                  <Text style={{alignSelf:'center', fontSize:50, color:COLOR.PUTIH}}>{userStore.username.toUpperCase().charAt(0)}</Text>:
                  <ImageBackground source={{uri: serverAPI+getProfilePhoto+this.state.image}} imageStyle={{borderRadius:100}} style={{height:100, width:100, resizeMode:'cover', justifyContent:'center'}}/>
                  }
                </View>
            </TouchableOpacity>
            <View style={{justifyContent:'flex-end'}}>
              <TouchableOpacity onPress={()=>{this.choosePhoto()}}>
                <Image source={edit} style={{width:20, height:20}}/>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={{flex:1, marginHorizontal:20}}>
          <Text style={{color:COLOR.HITAM_REDUP, fontSize:20, fontWeight:'bold', fontFamily:"sans-serif-medium"}}>{userStore.name}</Text>
          <Text style={{fontSize:17, fontFamily:"sans-serif-light"}}>@{userStore.username}</Text>
        </View>
        <View style={{marginHorizontal:20, marginBottom:10}}>
          <TouchableOpacity onPress={()=>{
            signOut(this.props.navigation)
            }}>
            <Text style={{fontSize:18}}>Logout</Text>
          </TouchableOpacity>
          {/* <TouchableOpacity onPress={()=>{
            this.uploadProfilePhoto()
            }}>
            <Text>Upload</Text>
          </TouchableOpacity> */}
        </View>
        {/* <TouchableOpacity onPress={()=>{
          this.setState({
            image: convertToByteArray(this.state.image)
          })
          }}>
          <Text>encode</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>{
          this.setState({
            image: convertToBase64(this.state.image)
          })
          }}>
          <Text>decode</Text>
        </TouchableOpacity> */}
        <ImageViewer close={()=>{this.setState({viewImage:false})}} load={this.state.viewImage} image={this.state.image} profilePhoto={true}/>
        <LoadingScreen load={this.state.loading}/>
      </View>
    );
  }

  viewImage(){
    this.setState({
      viewImage:true
    })
  }
}
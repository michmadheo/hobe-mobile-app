import React, { Component } from 'react';
import { Text, View, Button, TouchableOpacity, ColorPropType, BackHandler, Image } from 'react-native';
import Header from '../component/header';
import * as COLOR from '../src/colors';

import {fetchPOST} from '../functions/fetch';
import {HERO_LIST, demoAPI, appdescription} from '../api/api';

import demoStore from '../stores/demoStore';

import {signOut} from '../functions/firebaseFunctions/firebaseFunctions';
import userStore from '../stores/userStore';

import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Home from './home';
import Menu from './menu';
import ProfilePage from './profilePage';
import Activity from './activity';
import { home, searchWhite, unhome, addWhite, activity, unactivity, profile, unprofile, notif, unnotif } from '../src/images';

const Tab = createBottomTabNavigator();
const SearchComponent = () => {
  return null
}

export default class Dashboard extends Component {

  // static defaultProps ={
  //   initialRoute
  // }

  constructor(props) {
    super(props);
    this.state = {
      heroName: "Goblok",
      heroes: [],
      appName: "",
      appDesc: "",
    };
  }

  render() {
    return (
      <Tab.Navigator initialRouteName={this.props.route.params.initialRoute} tabBarOptions={{showLabel:false, activeTintColor:COLOR.TEMA_UTAMA, inactiveTintColor:COLOR.ABU_INPUT, labelStyle:{fontSize:15}, style:{height:50}}}>
          <Tab.Screen name="Home" component={Home} options={{tabBarIcon:({focused})=><Image source={focused?home:unhome} style={focused?iconBar:iconBarUn}/>}}/>
          <Tab.Screen name="Activity" component={Activity} options={{tabBarIcon:({focused})=><Image source={focused?activity:unactivity} style={focused?iconBar:iconBarUn}/>}}/>
          <Tab.Screen name="Add" component={SearchComponent} options={{tabBarIcon:()=>this.searchButton()}}/>
          <Tab.Screen name="Notification" component={Menu} options={{tabBarIcon:({focused})=><Image source={focused?notif:unnotif} style={focused?iconBar:iconBarUn}/>}}/>
          <Tab.Screen name="Profile" component={ProfilePage} options={{tabBarIcon:({focused})=><Image source={focused?profile:unprofile} style={focused?iconBar:iconBarUn}/>}}/>
      </Tab.Navigator>
    );
  }
  
  searchButton(){
    return(
      <TouchableOpacity 
      onPress={()=>{this.props.navigation.navigate('WriteNew')}} 
      style={{position:'absolute', 
      top:-16, 
      backgroundColor:COLOR.TEMA_UTAMA, 
      borderRadius:150,
      borderWidth:5,
      borderColor:COLOR.PUTIH,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      
      elevation: 3,}}
      >
        {/* <Text style={{padding:20}}>O</Text> */}
        <Image source={addWhite} style={{margin:15, width:20, height:20}}></Image>
      </TouchableOpacity>
    )
  }

  async getDemoAPI(){
    let request = {name:"wedew"}
    fetchPOST(appdescription, request).then((response)=>{
      let data = response
      // alert(JSON.stringify(response))
      this.setState({
        appName:response.message,
        appDesc:response.desc
      })
    })
  }
}

const navigationBar = {
  backgroundColor:COLOR.PUTIH, 
  height:60, width:'100%', 
  justifyContent:'center',
  shadowColor: "#000",
  shadowOffset: {                 
    width: 0,                
    height: 4,    
    },           
  shadowOpacity: 0.32,      
  shadowRadius: 5.46,
  elevation: 9,
}

let iconSize = 25

const iconBar = {
  height: iconSize,
  width: iconSize
}

const iconBarUn = {
  height: iconSize-5,
  width: iconSize-5
}
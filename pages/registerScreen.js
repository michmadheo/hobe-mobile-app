import React, { Component } from 'react';
import { Text, View, TouchableOpacity, TextInput, ScrollView, Image, ActivityIndicator} from 'react-native';
import HeaderBack from '../component/headerBack';
import userStore from '../stores/userStore';

import * as COLOR from '../src/colors';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { fetchPOST } from '../functions/fetch';

import {register} from '../api/api';
import { invisPass, visiblePass } from '../src/images';
import LoadingScreen from '../component/loadingScreen';

export default class Register extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: "",
      username: "",
      phone: "",
      email: "",
      password: "",
      reenterPassword:"",
      errname: "",
      errusername: "",
      errphone: "",
      erremail: "",
      errpassword: "",
      errreenterPassword:"",
      logged: null,
      passVisible: true,
      repassVisible: true,
      registerButton:false,
      passStrengh:"",
      passStrenghLeft:1,
      passStrenghRight:1,
      passStrenghLeftColor:COLOR.PUTIH,
      passStrenghRightColor:COLOR.PUTIH,
      loading: false
    };
  }

  componentDidMount(){
    //   alert('Yuhu')
    // this.loginCheck()
  }

  loginCheck(){
    firebase.auth().onAuthStateChanged(user =>{
      // alert(JSON.stringify(user))
      if(user){
        if(user.emailVerified){
          userStore.data = user
          this.props.navigation.replace('Dashboard')
        }
      }
    })
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor:COLOR.PUTIH}}>
        <HeaderBack title={"Sign-up"} 
        actionButton={()=>{this.props.navigation.pop()}}/>
        <ScrollView>
          <View style={{flex:1,marginHorizontal:25, marginTop:20}}>
              <Text style={{
                fontSize:25,
                marginBottom:20, 
                color:COLOR.HITAM_REDUP, 
                fontFamily:"sans-serif-light", 
                fontWeight:'bold'}}>
                  Let's get you onboard!
                </Text>
              <Text style={titleText}>Full Name</Text>
              <View style={formContainer}>
                <TextInput
                    secureTextEntry={false}
                    placeholder={"John Doe"}
                    style={formText}
                    onChangeText={(name) => 
                      this.setState({
                        name,
                        errname:""
                      })}
                    value={this.state.name}/>
              </View>
              <Text style={errText}>
                {this.state.errname}
              </Text>
              <Text style={titleText}>Username</Text>
              <View style={formContainer}>
                <TextInput
                    secureTextEntry={false}
                    placeholder={"JohnDoe123"}
                    style={formText}
                    onChangeText={(username) => this.setState({
                      username,
                      errusername:""
                    })}
                    value={this.state.username}/>
              </View>
              <Text style={errText}>
                {this.state.errusername}
              </Text>
              <Text style={titleText}>E-mail</Text>
              <View style={formContainer}>
                <TextInput
                    secureTextEntry={false}
                    placeholder={"johndoe@gmail.com"}
                    style={formText}
                    onChangeText={(email) => this.setState({
                      email,
                      erremail:""
                    })}
                    value={this.state.email}/>
              </View>
              <Text style={errText}>
                {this.state.erremail}
              </Text>
              <Text style={titleText}>Phone</Text>
              <View style={formContainer}>
                <TextInput
                    secureTextEntry={false}
                    keyboardType={"number-pad"}
                    placeholder={"085608080808"}
                    style={formText}
                    onChangeText={(phone) => this.setState({
                      phone,
                      errphone:""
                    })}
                    value={this.state.phone}/>
              </View>
              <Text style={errText}>
                {this.state.errphone}
              </Text>
              <Text style={titleText}>Password</Text>
              <View style={formContainer}>
                <TextInput
                    secureTextEntry={this.state.passVisible}
                    placeholder={"YourPass123"}
                    style={formText}
                    onChangeText={(password) =>{ 
                      this.calculatePasswordStrengh(password)
                      this.setState({
                      password,
                      errpassword:""
                      })
                  }}
                    value={this.state.password}/>
                    {this.state.password.length>0?
                      <TouchableOpacity style={{justifyContent:'center'}} 
                      onPress={()=>{
                        this.setState({
                          passVisible:!this.state.passVisible
                        })
                      }}
                      >
                        <Image source={this.state.passVisible?invisPass:visiblePass} style={{width:25, height:25, marginRight:20}}/>
                      </TouchableOpacity>:null}
              </View>
              {this.state.password.length > 0?
                <View style={{marginTop:5}}>
                  <View style={{flexDirection:'row', height:5}}>
                    <View style={{
                      flex:this.state.passStrenghLeft,
                      backgroundColor:this.state.passStrenghLeftColor, 
                      borderRadius:10
                      }}/>
                    <View style={{
                      flex:this.state.passStrenghRight,
                      backgroundColor:this.state.passStrenghRightColor,
                      borderTopEndRadius:10,
                      borderBottomEndRadius:10
                      }}/>
                  </View>
                    {/* <Text style={{color:COLOR.HITAM_REDUP}}>{this.state.passStrengh}</Text> */}
                  <View style={{backgroundColor:COLOR.ABU_TERANG, marginTop:5, borderRadius:5, padding:10}}>
                    <Text style={{
                      color:this.state.password.length >= 8?COLOR.TEMA_UTAMA:COLOR.MERAH
                      }}>Minimum 8 characters</Text>
                    <Text style={{
                      color:/^(?=.*\d)[A-Za-z\d]{0,}$/.test(this.state.password)?COLOR.TEMA_UTAMA:COLOR.MERAH
                      }}>Contain number</Text>
                    <Text style={{
                      color:/^(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\d]{0,}$/.test(this.state.password)?COLOR.TEMA_UTAMA:COLOR.MERAH
                      }}>Contain upper and lower case</Text>
                  </View>
                </View>:null
              }
              <Text style={errText}>
                {this.state.errpassword}
              </Text>
              {this.state.password !== ""?
              <View>
                <Text style={titleText}>Re-enter Password</Text>
              <View style={formContainer}>
                <TextInput
                    secureTextEntry={this.state.repassVisible}
                    placeholder={"Your Password"}
                    style={formText}
                    onChangeText={(reenterPassword) => this.setState({
                      reenterPassword,
                      errreenterPassword:""
                    })}
                    value={this.state.reenterPassword}/>
                    {this.state.reenterPassword.length>0?
                      <TouchableOpacity style={{justifyContent:'center'}} 
                      onPress={()=>{
                        this.setState({
                          repassVisible:!this.state.repassVisible
                        })
                      }}
                      >
                        <Image source={this.state.repassVisible?invisPass:visiblePass} style={{width:25, height:25, marginRight:20}}/>
                      </TouchableOpacity>:null}
              </View>
              <Text style={errText}>
                {this.state.errreenterPassword}
              </Text>
              </View>:null}
              {!this.state.registerButton?
              <TouchableOpacity 
              onPress={()=>{
                if(this.buttonPressable()){
                  if(this.validate()){
                    this.register()
                  } 
                }
              }} 
              style={{
                backgroundColor:this.buttonColor(),
                borderRadius:10,
                marginBottom:10,
                justifyContent:'center', 
                alignItems:'center'}}>
                  <Text style={{
                    marginVertical:8, 
                    fontSize:20, 
                    color:COLOR.PUTIH,
                    fontFamily:"sans-serif-light"}}>
                      Sign-up
                      </Text>
              </TouchableOpacity>:<ActivityIndicator style={{paddingBottom:10}} size="large" color="#00ff00" />
              }
          </View>
        </ScrollView>
        <LoadingScreen load={this.state.loading}/>
      </View>
    );
  }

  buttonColor(){
    if(
      this.state.name !== "" && 
      this.state.username !== "" &&
      this.state.email !== "" &&
      this.state.phone !== "" &&
      this.state.password !== "" &&
      this.state.reenterPassword !== ""){
      return COLOR.TEMA_UTAMA
    }
    else{
      return COLOR.ABU_INPUT
    }
  }
  buttonPressable(){
    if(
      this.state.name !== "" && 
      this.state.username !== "" &&
      this.state.email !== "" &&
      this.state.phone !== "" &&
      this.state.password !== "" &&
      this.state.reenterPassword !== ""){
      return true
    }
    else{
      return false
    }
  }

  calculatePasswordStrengh(password){
    if(password.length <= 2){
      this.setState({
        passStrenghLeft:0.2,
        passStrenghRight: 1,
        passStrenghLeftColor:COLOR.MERAH,
        passStrenghRightColor:COLOR.ABU_TERANG,
        passStrengh: "Your password is too weak"
      })
    }
    if(password.length >= 6){
      this.setState({
        passStrenghLeft:0.5,
        passStrenghRight: 1,
        passStrenghLeftColor:COLOR.MERAH,
        passStrenghRightColor:COLOR.ABU_TERANG,
        passStrengh: "Your password weak"
      })
    }
    if(password.length >= 8){
      this.setState({
        passStrenghLeft:1,
        passStrenghRight: 1,
        passStrenghLeftColor:COLOR.ORANYE_TERANG,
        passStrenghRightColor:COLOR.ABU_TERANG,
        passStrengh: "Your password is decent"
      })
    }
    if(password.length >= 10){
      this.setState({
        passStrenghLeft:1,
        passStrenghRight: 0.5,
        passStrenghLeftColor:COLOR.TEMA_UTAMA,
        passStrenghRightColor:COLOR.ABU_TERANG,
        passStrengh: "Your password is pretty strong"
      })
    }
    if(password.length >= 12){
      this.setState({
        passStrenghLeft:1,
        passStrenghRight: 0.2,
        passStrenghLeftColor:COLOR.TEMA_UTAMA,
        passStrenghRightColor:COLOR.ABU_TERANG,
        passStrengh: "Your password is so strong"
      })
    }
    if(password.length >= 15){
      this.setState({
        passStrenghLeft:1,
        passStrenghRight: 0,
        passStrenghLeftColor:COLOR.TEMA_UTAMA,
        passStrenghRightColor:COLOR.ABU_TERANG,
        passStrengh: "Your password is super saiyan"
      })
    }
  }

  validate(){
    let validation = true
    if(this.state.reenterPassword !== this.state.password){
      validation = false,
      this.setState({
        errreenterPassword:"The password doesn't Match"
      })
    }
    if(this.state.password !== ""){
      if(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(this.state.password)){
        this.setState({
          errpassword:""
        })
      }
      else{
        validation = false;
      }
    }
    if(this.state.password === ""){
      validation = false,
      this.setState({
        errpassword:"Your password cannot be empty"
      })
    }
    if(this.state.phone !== ""){
      if(/^08.*$/.test(this.state.phone) && this.state.phone.length >= 11){
        this.setState({
          errphone:""
        })
      }
      else{
        validation = false,
        this.setState({
          errphone:"Your phone number length is invalid"
        })
      }
    }
    if(this.state.phone === ""){
      validation = false,
      this.setState({
        errphone:"Your phone number cannot be empty"
      })
    }
    if(this.state.email !== ""){
      if(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(this.state.email)){
        this.setState({
          erremail:""})
      }
      else{
        validation = false,
        this.setState({
          erremail:'Your email is invalid'})
      }
    }
    if(this.state.email === ""){
      validation = false,
      this.setState({
        erremail:"Your email cannot be empty"
      })
    }
    if(this.state.username !== ""){
      if(this.state.username.length < 3 || this.state.username.length > 20){
        validation = false,
        this.setState({
          errusername: "Username characters: min. 3  max. 20"
        })
      }
      else{
        if(/^[_A-z0-9]*((-|\s)*[_A-z0-9])*$/.test(this.state.username)){
          this.setState({
            errusername:""
          })
        }
        else{
          validation = false,
          this.setState({
            errusername:"Your username cannot contain special characters"
          })
        }
      }
    }
    if(this.state.username === ""){
      validation = false,
      this.setState({
        errusername:"Your username cannot be empty"
      })
    }
    if(this.state.name !== ""){
      if(this.state.name.length < 3){
        validation = false,
        this.setState({
          errname:"Minimum Full name characters are 3"
        })
      }
      else{
        this.setState({
          errname:""
        })
      }
    }
    if(this.state.name === ""){
      validation = false,
      this.setState({
        errname:"Your full name cannot be empty"
      })
    }
    return validation
  }

  register(){
    this.setState({
      registerButton:true,
      erremail: "",
      errusername: "",
      loading: true,
    })
    let request = {
      name:this.state.name,
      username:this.state.username,
      email:this.state.email,
      phone: this.state.phone.toString(),
      password:this.state.password,
    }
    fetchPOST(register, request).then((response)=>{
      if(response.status === "SUCCESS"){
        this.setState({
          loading:false
        })
        this.props.navigation.navigate('SuccessPage', {message:response.message, goTo:'Main'})
      }
      else if(response.status === "TAKEN"){
        this.setState({
          erremail: response.messageEmail,
          errusername: response.messageUsername,
          registerButton: false,
          loading: false
        })
      }
      else{
        this.setState({
          loading:false
        })
        alert(response.message)
      }
    })
  }
}

//Styling

const formContainer={
  backgroundColor:COLOR.ABU_TERANG,
  borderColor:COLOR.TEMA_UTAMA,
  borderRadius:10,
  flexDirection:'row'
}

const formText={
  fontSize:15,
  marginVertical:2,
  flex:1
}

const titleText={
  fontSize:20, 
  color:COLOR.HITAM_REDUP, 
  fontFamily:"sans-serif-light"}

const errText={
  color:COLOR.MERAH,
  paddingBottom:5
}
import React, { Component } from 'react';
import { Text, View, TextInput, Image, Animated, BackHandler, TouchableOpacity } from 'react-native';
import count from '../functions/demoFunction';
import { fetchPOST } from '../functions/fetch';
import * as COLOR from '../src/colors';
import { search } from '../src/images';

import {searchAll} from '../api/api';
// import { TouchableOpacity } from 'react-native-gesture-handler';

export default class SearchPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      color: '#008f68',
      fadeHello: new Animated.Value(0),
      rotateIcon: new Animated.Value(0),
      searchBarPos: new Animated.Value(50),
      searchValue:"",
      searchResults: [],
    };
  }

  moveSearchBar = () =>{
    Animated.spring(this.state.searchBarPos, {
        toValue:1,
        useNativeDriver: true,
        bounciness:12,
    }).start()
  }

  rotateIconBar = () =>{
    Animated.timing(
      this.state.rotateIcon,
    {
      toValue: -1,
      duration: 500,
      useNativeDriver: true
    }
  ).start()
  }

  componentDidMount(){
    this.moveSearchBar()
    this.rotateIconBar()
    // BackHandler.addEventListener('hardwareBackPress', this.moveSearchBarBack)
  }

//   moveSearchBarBack = () =>{
//     Animated.spring(this.state.searchBarPos, {
//         toValue:60,
//         useNativeDriver: true,
//     }).start()
// }

  render() {
    const spin = this.state.rotateIcon.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })
    return (
      <View style={{flex:1, backgroundColor:COLOR.PUTIH}}>
        <View style={{marginHorizontal:20}}>
          <Animated.View 
          style={{
            height:50, 
            alignItems:'center', 
            marginTop:20, 
            backgroundColor:COLOR.ABU_TERANG, 
            borderRadius:50, flexDirection:'row',
            transform: [{translateY:this.state.searchBarPos}]}}>
            <Animated.Image source={search} style={{transform: [{rotate: spin}], width:20, height:20, marginLeft:20, opacity:0.7}}/>
            <TextInput
              placeholder="Let's start discovering..."
              onChangeText={(searchValue)=>{
                this.searchItems(searchValue)
                this.setState({
                searchValue})
              }
              }
              style={{paddingLeft:10, color:COLOR.HITAM_REDUP, opacity:0.7, fontSize:15}}
              value={this.state.searchValue}
              autoFocus={true}
            />
            {/* <Text style={{paddingLeft:10, color:COLOR.HITAM_REDUP, opacity:0.7, fontSize:17}}>Let's start discovering...</Text> */}
          </Animated.View>
          {/* <Text>{this.state.data.length}</Text> */}
          {this.state.searchResults.map((item,index)=>{
            return(
              <TouchableOpacity key={index} onPress={()=>{alert('This will bring you to the '+item.title)}}>
                <Text style={{fontSize:20, paddingLeft:20, paddingVertical:10}}>{item.title}</Text>
                <View style={{height:1, backgroundColor:COLOR.ABU_INPUT}}/>
              </TouchableOpacity>
            )})
          }
        </View>
      </View>
    );
  }

  searchItems(searchValue){
    if(searchValue.length > 0){
      let request = {
        search : searchValue.toLowerCase()
      }
      // alert(JSON.stringify(request))
      fetchPOST(searchAll, request).then((response)=>{
        if(response.status === "SUCCESS"){
          // alert(JSON.stringify(response.data))
          this.setState({
            searchResults: response.data.topic
          })
        }
      })
    }
    else{
      this.setState({
        searchResults: []
      })
    }
  }
}
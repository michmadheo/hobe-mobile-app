import React, { Component } from 'react';
import { Text, View, TouchableOpacity, TextInput, ScrollView, RefreshControl, Image, ActivityIndicator, ImageBackground } from 'react-native';
import {store} from '../functions/astore';
import {signOut} from '../functions/userFunction';
import Header from '../component/headerBack';
import * as COLOR from '../src/colors';
import userStore from '../stores/userStore';
import { fetchPOST } from '../functions/fetch';
import { getSinglePost, postComment, postReply, deletePost, getProfilePhoto, getImage } from '../api/api';
import LoadingScreen from '../component/loadingScreen';
import { cancelWhite, comment, reply, heart, heartFilled, edit } from '../src/images';
import { formatDateForActivity } from '../functions/generalFunctions';
import ActivtyActionModal from '../component/activityActionModal';
import ConfirmationModal from '../component/confirmationModal';
import { serverAPI } from '../server-configuration/server';

export default class CommentPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id:this.props.route.params.id,
      postedBy:"",
      creatorId:"",
      commentText:"",
      comments:[],
      image:"",
      title:"",
      topic: "",
      content:"",
      createOn:"",
      profilePhoto:"",
      loading:true,
      refresh:false,
      replyId:"",
      replyTarget:"",
      autoFocus:false,
      contentController:[],
      posting: false,
      loved: false,
      postActionModal: false,
      confirmationModal: false,
    };
  }

  componentDidMount(){
    // alert(this.state.id)
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.getPostDetail()
    });
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor:COLOR.PUTIH}}>
        {!this.state.loading?
        <Header title={'Comment on '+this.state.postedBy+"'s post"} actionButton={()=>{this.props.navigation.goBack()}}/>
        :null}
          {!this.state.loading?
                    <View style={{flex:1, paddingHorizontal:10, marginTop:10}}>
                    <ScrollView showsVerticalScrollIndicator={false} refreshControl={
                      <RefreshControl refreshing={this.state.refresh} onRefresh={()=>{this.getPostDetail()}}/>
                    }>
                      <View style={{flexDirection:'row', paddingBottom:10}}>
                        <View style={{flexDirection:'row', flex:1}}>
                          <View style={{height:40, width:40, backgroundColor:COLOR.ABU_INPUT, borderRadius:50, justifyContent:'center', marginRight:10, marginTop:2}}>
                            {this.state.profilePhoto?
                            <ImageBackground source={{uri: serverAPI+getProfilePhoto+this.state.profilePhoto}} imageStyle={{borderRadius:50}} style={{height:40, width:40, resizeMode:'cover', justifyContent:'center'}}/>:
                            <Text style={{alignSelf:'center', fontSize:20, color:COLOR.PUTIH}}>{this.state.postedBy.toUpperCase().charAt(0)}</Text>
                            }
                          </View>
                          <View style={{marginLeft: 2, justifyContent:'center'}}>
                            <Text style={{color:COLOR.HITAM_REDUP, fontFamily:"sans-serif-medium", fontSize:15}}>{this.state.postedBy}</Text>
                            <Text style={{color:COLOR.HITAM_REDUP, fontFamily:"sans-serif-light", fontSize:15}}>{this.state.topic}</Text>
                          </View>
                        </View>
                        {this.state.creatorId === userStore.id?
                        <View style={{justifyContent:'center'}}>
                        <TouchableOpacity onPress={()=>{
                          this.setState({
                            postActionModal:true
                          })
                        }}>
                          <Image source={edit} style={{width:20 , height:20}}/>
                        </TouchableOpacity>
                      </View>:null
                        }
                      </View>
                      {/* <Text style={{fontSize:15, opacity:0.8}}>{this.state.topic}</Text> */}
                      <Text style={{fontWeight:'bold', fontSize:18, color:COLOR.HITAM_REDUP}}>{this.state.title}</Text>
                      <Text style={{fontSize:15, marginTop:5}}>{this.state.content}</Text>
                      {
                        this.state.image?
                        <Image source={{uri: serverAPI+getImage+this.state.image}} style={{resizeMode:'contain', height:300, width:'100%'}}/>:null
                      }
                      <Text style={{color:COLOR.HITAM_REDUP, opacity:0.8}}>{formatDateForActivity(this.state.createOn)}</Text>
                      <TouchableOpacity onPress={()=>{
                        this.setState({
                          loved: !this.state.loved
                        })
                      }} style={{marginTop:10}}>
                        <Image source={this.state.loved?heartFilled:heart} style={{marginLeft:5, height:25, width:25}}/>
                      </TouchableOpacity>
                      <View style={{height:2, backgroundColor:COLOR.ABU_TERANG, marginTop:10}} />
                      {this.state.comments.map((item,index)=>{
                        return(
                          <View key={index} style={{flexDirection:'row', marginTop:15}}>
                            <View style={{height:35, width:35, backgroundColor:COLOR.ABU_INPUT, borderRadius:50, justifyContent:'center', marginRight:10, marginTop:5}}>
                              {item.creator.profilePhoto?
                                <ImageBackground source={{uri: serverAPI+getProfilePhoto+item.creator.profilePhoto}} imageStyle={{borderRadius:50}} style={{height:35, width:35, resizeMode:'cover', justifyContent:'center'}}/>:
                                <Text style={{alignSelf:'center', fontSize:20, color:COLOR.PUTIH}}>{item.creator.username.toUpperCase().charAt(0)}</Text>
                              }
                            </View>
                            <View style={{flex:1}}>
                              <Text style={{fontSize:15, fontWeight:'bold', marginBottom:2, color:COLOR.HITAM_REDUP}}>{item.creator.username}</Text>
                              <Text style={{fontSize:15}}>{item.content}</Text>
                              <Text style={{color:COLOR.HITAM_REDUP, opacity:0.8}}>{formatDateForActivity(item.createOn)}</Text>
                              <View style={{flexDirection:'row', marginTop:15}}>
                                <TouchableOpacity onPress={()=>{this.setReplyId(item._id, item.creator.username)}} style={{flexDirection:'row'}}>
                                  <Image source={reply} style={{height:22, width:22}}/>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>{this.controlReply(index)}} style={{flexDirection:'row', marginLeft:15}}>
                                  {/* <Image source={comment} style={{height:20, width:20}}/>
                                  <Text style={{marginLeft:5, fontSize:15}}>{item.comments.length}</Text> */}
                                  {item.comments.length > 0?
                                    <View style={{flexDirection:'row'}}>
                                      <Image source={comment} style={{height:20, width:20}}/>
                                      <Text style={{marginLeft:5, fontSize:15}}>{item.comments.length}</Text>
                                    </View>:null
                                  }
                                </TouchableOpacity>
                              </View>
                              {this.state.contentController[index]?
                              <View>
                              {item.comments.map((item, index,)=>{
                                return(
                                  <View key={index} style={{flexDirection:'row', marginTop:15}}>
                                      <View style={{height:35, width:35, backgroundColor:COLOR.ABU_INPUT, borderRadius:50, justifyContent:'center', marginRight:10, marginTop:5}}>
                                        {item.creator.profilePhoto?
                                          <ImageBackground source={{uri: serverAPI+getProfilePhoto+item.creator.profilePhoto}} imageStyle={{borderRadius:50}} style={{height:35, width:35, resizeMode:'cover', justifyContent:'center'}}/>:
                                          <Text style={{alignSelf:'center', fontSize:20, color:COLOR.PUTIH}}>{item.creator.username.toUpperCase().charAt(0)}</Text>
                                        }
                                      </View>
                                      <View style={{flex:1}}>
                                        <Text style={{fontSize:15, fontWeight:'bold', marginBottom:2, color:COLOR.HITAM_REDUP}}>{item.creator.username}</Text>
                                        <Text style={{fontSize:15}}>{item.content}</Text>
                                        <Text style={{color:COLOR.HITAM_REDUP, opacity:0.8}}>{formatDateForActivity(item.createOn)}</Text>
                                        <TouchableOpacity onPress={()=>{this.setReplyId(item.postId, item.creator.username)}} style={{flexDirection:'row', marginTop:2}}>
                                          <Image source={reply} style={{height:22, width:22}}/>
                                        </TouchableOpacity>
                                      </View>
                                    </View>
                                  )
                              })}
                            </View>:null}
                            </View>
                          </View>
                        )
                      })}
                    </ScrollView>
                  </View>:null}
          {!this.state.loading?
          <View style={{marginTop:5, paddingBottom:10}}>
          {this.state.replyId !==""?
            <View style={{backgroundColor:COLOR.TEMA_UTAMA, flexDirection:'row'}}>
              <Text style={{flex:1, color:COLOR.PUTIH, paddingVertical:10, marginLeft:10, fontSize:15}}>You're replying to {this.state.replyTarget}</Text>
              <TouchableOpacity onPress={()=>{this.unsetReplyId()}} style={{justifyContent:'center', marginRight:10}}>
                <Image source={cancelWhite} style={{height:20, width:20}}/>
              </TouchableOpacity>
            </View>:null
          }
          <View style={{height:2, backgroundColor:COLOR.ABU_TERANG, marginBottom:5}}/>
          <View style={{flexDirection:'row', marginLeft:10}}>
              <View style={{justifyContent:'center'}}>
                  <View style={{height:35, width:35, backgroundColor:COLOR.ABU_INPUT, borderRadius:50, justifyContent:'center'}}>
                    {userStore.profilePhoto?
                      <ImageBackground source={{uri: serverAPI+getProfilePhoto+userStore.profilePhoto}} imageStyle={{borderRadius:50}} style={{height:35, width:35, resizeMode:'cover', justifyContent:'center'}}/>:
                      <Text style={{alignSelf:'center', fontSize:20, color:COLOR.PUTIH}}>{userStore.username.toUpperCase().charAt(0)}</Text>
                    }
                  </View>
              </View>
              <View style={{flex:1}}>
                  <TextInput
                  placeholder={"post a comment as "+userStore.username}
                  multiline={true} 
                  autoFocus={this.state.autoFocus}
                  onChangeText={(commentText)=>{
                      this.setState({
                      commentText})
                  }
                  }
                  style={{color:COLOR.HITAM_REDUP, fontSize:15, paddingLeft:10, height:50}}
                  value={this.state.commentText}
                  />
              </View>
              <View style={{justifyContent:'center', marginRight:15}}>
                  <TouchableOpacity 
                  onPress={()=>{
                    if(this.state.commentText !== ""){
                      this.setState({
                        posting:true
                      })
                      if(this.state.replyId === ""){
                        this.postComment()
                      }
                      else{
                        this.postReply()
                      }
                    }
                    }}>
                      {this.state.posting?
                        <ActivityIndicator size="large" color={COLOR.TEMA_UTAMA} />:
                        <Text style={{color:COLOR.TEMA_UTAMA, fontSize:18}}>Post</Text>
                      }
                  </TouchableOpacity>
              </View>
          </View>
        </View>:null}
        <ActivtyActionModal 
        load={this.state.postActionModal}
        close={()=>{
          this.setState({
            postActionModal: false
          })
        }}
        yourPost={true}
        actionEdit={()=>{
          this.setState({
            postActionModal:false
          })
          this.props.navigation.navigate('EditPost', {
            title:this.state.title, 
            content:this.state.content, 
            postId:this.state.id,
            singlePage:true,
          })
        }}
        actionDelete={()=>{
          this.setState({
            postActionModal:false,
            confirmationModal:true,
          })
        }}
        />
        <ConfirmationModal 
        text={"Confirm Delete?"}
        load={this.state.confirmationModal}
        close={()=>{
          this.setState({
            confirmationModal: false
          })
        }}
        actionCancel={()=>{
          this.setState({
            confirmationModal:false
          })
        }}
        actionAccept={()=>{
          this.deletePost()
        }}
        />
        <LoadingScreen load={this.state.loading}/>
      </View>
    );
  }

  setReplyId(replyId, replyTarget){
    this.setState({
      replyId:replyId,
      replyTarget: replyTarget,
    })
  }

  controlReply(itemIndex){
    let array = this.state.contentController
    array.splice(itemIndex,1,!array[itemIndex])
    this.setState({
      contentController:array
    })
  }

  unsetReplyId(){
    this.setState({
      replyId:"",
      replyTarget:"",
      commentText:""
    })
  }

  postComment(){
    let request={
      username:userStore.username,
      post_id:this.state.id,
      comment:this.state.commentText
    }
    fetchPOST(postComment, request).then((response)=>{
      if(response.status === "SUCCESS"){
        this.getPostDetail()
        this.setState({
          commentText:"",
          posting: false
        })
      }
      else{
        alert(response.message)
        this.props.navigation.replace('Dashboard', {initialRoute:"Activity"})
        this.setState({
          posting: false
        })
      }
    })
  }

  postReply(){
    let request={
      username:userStore.username,
      post_id:this.state.replyId,
      comment:this.state.commentText
    }
    fetchPOST(postReply, request).then((response)=>{
      if(response.status === "SUCCESS"){
        this.getPostDetail()
        this.setState({
          commentText:"",
          posting: false
        })
        this.unsetReplyId()
      }
      else{
        alert(response.message)
        this.props.navigation.replace('Dashboard', {initialRoute:"Activity"})
        this.setState({
          posting: false
        })
      }
    })
  }

  getPostDetail(){
      let request={
          username:userStore.username,
          id:this.state.id
      }
      fetchPOST(getSinglePost, request).then((response)=>{
          if(response.status === "SUCCESS"){
              // alert(response.data.comments.length)
              this.setState({
                postedBy:response.data.creator.username,
                title: response.data.title,
                topic: response.data.topic.title,
                content: response.data.content,
                comments: response.data.comments,
                createOn: response.data.createOn,
                profilePhoto: response.data.creator.profilePhoto,
                creatorId: response.data.creator._id,
                image: response.data.image,
                contentController: [],
                loading:false
              },()=>{
                for(i=0; i<this.state.comments.length; i++){
                  this.state.contentController.push(false)
                }
              })
          }
          else{
            this.props.navigation.replace('Dashboard', {initialRoute:"Activity"})
            this.setState({loading:false})
            alert(response.message)
          }
      })
  }

  deletePost(){
    this.setState({
      confirmationModal:false,
      loading: true
    })
    let request = {
      username:userStore.username,
      post_id: this.state.id
    }
    fetchPOST(deletePost, request).then((response)=>{
      if(response.status === 'SUCCESS'){
        // alert(response.message)
        this.setState({
          loading:false
        },()=>{
          this.props.navigation.replace('Dashboard', {initialRoute:"Activity"})
        })
      }
      else{
        alert(response.message)
        this.setState({
          loading:false
        })
      }
    })
  }
}
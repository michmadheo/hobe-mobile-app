import {backToMain} from '../navigator/screenNavigator';
import {store} from '../functions/astore';
import userStore from '../stores/userStore';

export function signOut(nav){
    store('del','loginCred');
    userStore.name = ""
    userStore.username = ""
    userStore.email = ""
    userStore.phone = ""
    userStore.token = ""
    userStore.id = ""
    userStore.profilePhoto = ""
    backToMain(nav);
}
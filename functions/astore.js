import AsyncStorage from '@react-native-community/async-storage';

export async function store(action, key, item){
    // alert('wew')
    if(action === 'get'){
        try {
            const value = await AsyncStorage.getItem(key)
            if(value !== null) {
              return value
            }
            else{
                return null
            }
          } catch(e) {
          }
    }
    if(action === 'set'){
        try {
            await AsyncStorage.setItem(key, item)
          } catch (e) {
          }
    }
    if(action === 'del'){
      try {
        await AsyncStorage.removeItem(key)
      } catch(e) {
      }
    }
}
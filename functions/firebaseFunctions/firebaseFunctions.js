import React, { Component } from 'react';
import { Text, View } from 'react-native';
import firebase from 'firebase';
import userStore from '../../stores/userStore';

var firebaseConfig = {
    apiKey: "AIzaSyAx75cP25pMo3TY1nJKXBk-vBg1S8FE8yA",
    authDomain: "experiment-ae699.firebaseapp.com",
    databaseURL: "https://experiment-ae699.firebaseio.com",
    projectId: "experiment-ae699",
    storageBucket: "experiment-ae699.appspot.com",
    messagingSenderId: "10731287618",
    appId: "1:10731287618:web:46376ceaf497929ed4a9db",
    measurementId: "G-7NGCX0Z3MY"
  };

export function initializeFirebase(){
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
     }
};

export function signOut(action){
  action()
  firebase.auth().signOut()
  userStore.data = []
}
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { serverAPI } from '../server-configuration/server';

export async function fetchPOST(url, request){
  console.log('url:', serverAPI+url)
  console.log('request:', request)
  let endpoint = await serverAPI+url
  if(endpoint !== undefined){
    try {
      let response = await fetch(endpoint, {
        method:'POST', 
        headers:{'Content-Type':'application/json'}, 
        body:JSON.stringify(request)});
      let json = await response.json();
      // alert(JSON.stringify(json))
      console.log('response:', json)
      return json;
    } catch (error) {
      alert(error)
      console.log('error when calling:', url);
    }
  }
  else{
    console.log('No API URL defined')
  }
}
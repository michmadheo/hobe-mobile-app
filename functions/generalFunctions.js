import React, { Component } from 'react';
import { Text, View } from 'react-native';

export function randomColor(){
    const randomColor = '#'+Math.floor(Math.random()*16777215).toString(16);
    return randomColor
};

export function moveTo(targetScreen){
  alert(targetScreen)
}

export function formatDateForActivity(date){
  const nowDate = new Date().getTime()
  let time = ""
  let days =['sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
  let months =['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
  let interval = nowDate - date
  if(interval < 60000){
    let minutes = Math.floor(interval / 1000);
    if(minutes <= 0){
      time = "0s ago"
      return time
    }
    else{
      time = minutes+"s ago"
      return time
    }
  }
  if(interval > 60000 && interval < 3600000){
    let minutes = Math.floor(interval / 60000);
    time = minutes+"m ago"
    return time
  }
  if(interval > 3600000 && interval < 86400000){
    let minutes = Math.floor(interval / 3600000);
    time = minutes+"h ago"
    return time
  }
  if(interval > 86400000 && interval < 172800000){
    let minutes = Math.floor(interval / 86400000);
    time = minutes+"d ago"
    return time
  }
  if(interval > 172800000){
    let postDate = new Date(date);
    let getDate = new Date(nowDate);
    let currYear = getDate.getFullYear();
    let day = postDate.getDate();
    let month = months[postDate.getMonth()];
    let year = postDate.getFullYear();
    if(year === currYear){
      time = day+" "+month
      return time
    }
    else{
      time = day+" "+month+", "+year
      return time 
    }
    
    // return postDate.toString()
  }
}
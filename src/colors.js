export const PUTIH = "#ffffff";
export const HITAM = "#000000";
export const HITAM_REDUP = "#1f1f1f";
export const ABU_INPUT = "#e1e1e1";
export const ABU_TERANG = "#f7f7f7";
export const MERAH = '#ff3535';
export const ORANYE_TERANG = '#ffb164';
export const TEMA_UTAMA = '#57d134';
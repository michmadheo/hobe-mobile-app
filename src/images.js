export const backIcon = require('./icons/backIcon.png');
export const backIconWhite = require('./icons/backIconWhite.png');
export const dropDown = require('./icons/dropDown.png');
export const invisPass = require('./icons/invisiblePass.png');
export const visiblePass = require('./icons/visiblePass.png');
export const hobeLogo = require('./logo/hobeLogoShort.png');
export const hobeLogoWhite = require('./logo/hobeLogoShortWhite.png');
export const comment = require('./icons/comment.png');
export const reply = require('./icons/reply.png');
export const heart = require('./icons/heart.png');
export const heartFilled = require('./icons/heartFilledAlt.png');
export const edit = require('./icons/edit.png');
export const more = require('./icons/more.png');

export const home = require('./icons/home.png');
export const unhome = require('./icons/unhome.png');
export const activity = require('./icons/activity.png');
export const unactivity = require('./icons/unactivity.png');
export const notif = require('./icons/notif.png');
export const unnotif = require('./icons/unnotif.png');
export const profile = require('./icons/profile.png');
export const unprofile = require('./icons/unprofile.png');

export const addWhite = require('./icons/add.png');
export const addPhoto = require('./icons/addPhoto.png');
export const search = require('./icons/search.png');
export const cancel = require('./icons/cancel.png');
export const cancelWhite = require('./icons/cancelWhite.png');

export const paperPlane_Anim = require('./animated/paperPlane.json');
export const check_Anim = require('./animated/check.json');
export const gardening_Anim = require('./animated/gardening.json');
export const skating_Anim = require('./animated/skating.json');
export const cooking_Anim = require('./animated/cooking.json');
export const watching_Anim = require('./animated/watching.json');
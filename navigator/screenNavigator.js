import React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Splashscreen from '../pages/splashScreen';
import Dashboard from '../pages/dashboard';
import Menu from '../pages/menu';
import Activity from '../pages/activity';
import LoginScreen from '../pages/loginScreen';
import Welcome from '../pages/welcome';
import Register from '../pages/registerScreen';
import SuccessPage from '../pages/successPage';

import SearchPage from '../pages/searchPage';
import WriteNew from '../pages/writeNew';
import EditPost from '../pages/editPost';
import CommentPage from '../pages/commentPage';

import ProfilePage from '../pages/profilePage';

const stack = createStackNavigator();

export default function ScreenNavigator() {
  return (
      <NavigationContainer>
          <stack.Navigator headerMode={false} mode={'card'} >
            <stack.Screen name="Splashscreen" component={Splashscreen} />
            <stack.Screen name="Main" component={Welcome} />
            <stack.Screen name="Register" component={Register} />
            <stack.Screen name="Login" component={LoginScreen} />
            <stack.Screen name="Dashboard" component={Dashboard} initialParams={{initialRoute:'Home'}} />
            <stack.Screen name="Activity" component={Activity} />
            <stack.Screen name="Menu" component={Menu} />
            <stack.Screen name="SearchPage" component={SearchPage} options={{animationEnabled:false}} />
            <stack.Screen name="SuccessPage" component={SuccessPage} />
            <stack.Screen name="WriteNew" component={WriteNew} />
            <stack.Screen name="CommentPage" component={CommentPage} />
            <stack.Screen name="EditPost" component={EditPost} />
            <stack.Screen name="ProfilePage" component={ProfilePage} />
          </stack.Navigator>
      </NavigationContainer>
  );
}

export function backToMain(nav){
  nav.replace('Main')
}
